#ifndef _VENGINE_GAMELOOP
#define _VENGINE_GAMELOOP

class GameLoop
{
private:
    static inline bool isRunning;
    static inline float deltaTime;
    static bool (*const subsystemStarts[])();
    static void (*const subsystemUpdates[])();
    static void (*const subsystemStops[])();

public: 
    /// @brief Initializes everything and starts the game loop.
    static void StartLoop();
    /// @brief Ends the game loop and stops all initialized components.
    static void EndLoop();
    /// @brief Returns if the game loop is running.
    static bool GetState();
    /// @brief Returns time in seconds that has passed since the last update.
    static float GetDeltaTime();
};

#endif