template <class Derieved, bool RequireInstance>
void SceneSingleton<Derieved, RequireInstance>::SetInstance(bool override)
{
    Derieved *component = static_cast<Derieved*>(this);
    if (!override && component->GetScene()->template GetInstance<Derieved>())
    {
        SDL_LogWarn(0, std::format("{} instance already exists on current scene.", get_type_name<Derieved>()).c_str());
        return;
    }
    component->GetScene()->template SetInstance<Derieved>(component->GetPointer().template StaticCast<Derieved>());
}

template <class Derieved, bool RequireInstance>
bool SceneSingleton<Derieved, RequireInstance>::IsInstance() const
{
    const Derieved *component = static_cast<const Derieved*>(this);
    return GetInstance() == component->GetPointer();
}

template <class Derieved, bool RequireInstance>
Pointer<Derieved> SceneSingleton<Derieved, RequireInstance>::GetInstance()
{   
    return ObjectManager::GetCurrentScene()->GetInstance<Derieved>();
}

template <class Derieved, bool RequireInstance>
Pointer<Derieved> SceneSingleton<Derieved, RequireInstance>::GetInstance(const Pointer<Scene> &scene)
{
    auto instance = scene->GetInstance<Derieved>();
    if constexpr (RequireInstance)
    {
        if (!instance)
        {
            SDL_LogError(0, std::format("Missing {} instance.", get_type_name<Derieved>()).c_str());
        }
    }
    return instance;
}