#ifndef _VENGINE_MIXINS_SHARED_POINTER_
#define _VENGINE_MIXINS_SHARED_POINTER_

#include "../pointer.h"

/// @brief Mixin for classes that hold a publicly readable `Pointer` to themselves.
template <class T>
class SharedPointer
{
private:
    const Pointer<T> ptr;

public:
    SharedPointer();

    /// @brief Returns a shared pointer for this object. No other way of getting a pointer should be used. 
    const Pointer<T> &GetPointer();
    /// @brief Returns a shared pointer for this object. No other way of getting a pointer should be used.
    Pointer<const T> GetPointer() const;
};

#include "sharedpointer.hxx"

#endif