template <class T>
SharedPointer<T>::SharedPointer() : ptr(static_cast<T*>(this)) {}

template <class T>
const Pointer<T> &SharedPointer<T>::GetPointer()
{
    return ptr;
}

template <class T>
Pointer<const T> SharedPointer<T>::GetPointer() const
{
    return ptr;
}
