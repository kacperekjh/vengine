#ifndef _VENGINE_MIXINS_SCENESINGLETON_
#define _VENGINE_MIXINS_SCENESINGLETON_

#include "../gameobjects/objectmanager.h"
#include <format>
#include "../helper.h"

/// @brief Mixin for components that should have only a single instance per scene.
/// @tparam Derieved type of the component
/// @tparam RequireInstance dictates whether errors should by logged by `GetInstance` functions when there's no instance.
template <class Derieved, bool RequireInstance = true>
class SceneSingleton
{
protected:
    /// @brief Sets the scene instance of the component to this object.
    ///        Doesn't do anything if there's already an instance, unless `override` is true.
    void SetInstance(bool override = false);

public:
    /// @brief Returns if this component is the scene instance.
    bool IsInstance() const;
    /// @brief Returns the instance of this component on current scene.
    static Pointer<Derieved> GetInstance();
    /// @brief Returns the instance of this component on `scene`.
    static Pointer<Derieved> GetInstance(const Pointer<Scene> &scene);
};

#include "scenesingleton.hxx"

#endif