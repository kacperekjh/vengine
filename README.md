# VEngine
A small 2D/3D vector game engine written in C++.

## Requirements
SDL2, SDL2_ttf, SDL2_mixer, glm and OpenGL libraries.