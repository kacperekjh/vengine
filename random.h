#ifndef _VENGINE_RANDOM_
#define _VENGINE_RANDOM_

#include <random>
#include <concepts>

template <class T>
concept Numeric = std::is_integral_v<T> || std::is_floating_point_v<T>;

class Random
{
private:
    static inline std::random_device dev;
    static inline std::mt19937 gen = std::mt19937(dev());

public:
    /// @brief Returns a random number in range [from, to].
    /// @tparam T Numeric type
    template <Numeric T>
    static T GetRandomNumber(T from, T to);
    /// @brief Returns a random boolean.
    static bool GetRandomBool();
};

#include "random.hxx"

#endif