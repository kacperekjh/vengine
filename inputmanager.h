#ifndef _COMPONENTS_INPUTMANAGER
#define _COMPONENTS_INPUTMANAGER

#include <SDL2/SDL.h>
#include <functional>
#include <list>

class InputManager
{
private:
    using Callback = std::pair<SDL_Scancode, std::function<void()>>;

    inline static Uint8 *oldPressed;
    inline static const Uint8 *pressed;
    inline static int size;
    inline static std::list<Callback> pressCallbacks;
    inline static std::list<Callback> holdCallbacks;
    inline static std::list<Callback> releaseCallbacks;

    static bool Start();
    static void Stop();
    static void Update();

public:
    /// @brief Checks if `key` was pressed on this frame. Returns false if it's been held for multiple frames. 
    static bool IsPressed(SDL_Scancode key);
    /// @brief Checks if `key` is being held.
    static bool IsHeld(SDL_Scancode key);
    /// @brief Checks if key has been released ie. was pressed on the previous frame, but not on this.
    static bool IsReleased(SDL_Scancode key);
    /// @brief Registers `callback` and calls it everytime `key` is pressed.
    /// @return Function to unregister the callback.
    static std::function<void()> RegisterPressCallback(SDL_Scancode key, const std::function<void()> &callback);
     /// @brief Registers `callback` and calls it on every frame when `key` is held.
    /// @return Function to unregister the callback.
    static std::function<void()> RegisterHoldCallback(SDL_Scancode key, const std::function<void()> &callback);
    /// @brief Registers `callback` and calls it everytime `key` is released.
    /// @return Function to unregister the callback.
    static std::function<void()> RegisterReleaseCallback(SDL_Scancode key, const std::function<void()> &callback);

    friend class GameLoop;
};

#endif