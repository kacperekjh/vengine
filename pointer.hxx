template <class T>
Pointer<T>::Pointer(PointerData *data, T *ptr) : ptr(ptr), data(data)
{
    Increment();
}

template <class T>
void Pointer<T>::Increment()
{
    if (data)
    {
        data->references++;
    }
}

template <class T>
void Pointer<T>::Decrement()
{
    if (data && --(data->references) == 0)
    {
        delete data;
        data = nullptr;
    }
}

template <class T>
void Pointer<T>::SingleDelete(void *ptr)
{
    delete (T*)ptr;
}

template <class T>
void Pointer<T>::ArrayDelete(void *ptr)
{
    delete[] (T*)ptr;
}

template <class T>
template <PointerCastable<T> T2>
Pointer<T>::Pointer(T2 *ptr, bool isArray) : ptr(ptr), data(new PointerData{1, ptr == nullptr, isArray ? Pointer::ArrayDelete : Pointer::SingleDelete}) {}

template <class T>
template <PointerCastable<T> T2>
Pointer<T>::Pointer(T2 *ptr, void (*deleter)(void *ptr)) : ptr(ptr), data(new PointerData{1, ptr == nullptr, deleter}) {}

template <class T>
Pointer<T>::Pointer(const Pointer<T> &b) : ptr(b.ptr), data(b.data)
{
    Increment();
}

template <class T>
Pointer<T>::Pointer(std::nullptr_t) : ptr(nullptr), data(nullptr) {}

template <class T>
Pointer<T>::Pointer() : ptr(nullptr), data(nullptr) {}

template <class T>
Pointer<T>::~Pointer()
{
    Decrement();
}

template <class T>
T &Pointer<T>::operator*() const
{
    return *ptr;
}

template <class T>
T *Pointer<T>::operator->() const
{
    return ptr;
}

template <class T>
T &Pointer<T>::operator[](size_t index) const
{
    return ptr[index];
}

template <class T>
Pointer<T> &Pointer<T>::operator=(const Pointer<T> &b)
{
    if (&b != this)
    {
        Decrement();
        ptr = b.ptr;
        data = b.data;
        Increment();
    }
    return *this;
}

template <class T>
Pointer<T>::operator bool() const
{
    return ptr && !data->deleted;
}

template <class T>
template <PointerCastable<T> T2>
Pointer<T>::operator Pointer<T2>() const
{
    return Pointer<T2>(data, (T2*)ptr);
}

template <class T>
Pointer<T>::operator T*() const
{
    return ptr;
}

template <class T>
template <class T2>
bool Pointer<T>::operator==(const Pointer<T2> &b) const
{
    return ptr == b.ptr;
}

template <class T>
template <class T2>
bool Pointer<T>::operator==(T2 *b) const
{
    return ptr == b;
}

template <class T>
bool Pointer<T>::GetState() const
{
    return ptr && !data->deleted;
}

template <class T>
void Pointer<T>::Delete()
{
    if (ptr && !data->deleted)
    {
        data->deleter(ptr);
        data->deleted = true;
    }
}

template <class T>
template <class T2>
Pointer<T2> Pointer<T>::StaticCast() const
{
    return Pointer<T2>(data, static_cast<T2*>(ptr));
}

template <class T>
template <class T2>
Pointer<T2> Pointer<T>::DynamicCast() const
{
    return Pointer<T2>(data, dynamic_cast<T2*>(ptr));
}

template <class T>
template <class T2>
Pointer<T2> Pointer<T>::ReinterpretCast() const
{
    return Pointer<T2>(data, reinterpret_cast<T2*>(ptr));
}