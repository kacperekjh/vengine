template <Numeric T>
T Random::GetRandomNumber(T from, T to)
{
    using distribution = std::conditional_t<std::is_integral_v<T>, std::uniform_int_distribution<T>, std::uniform_real_distribution<T>>;
    auto dist = distribution(from, to);
    return dist(gen);
}