#ifndef _VENGINE_AUDIO_MUSIC_
#define _VENGINE_AUDIO_MUSIC_

#include "../resources/rwopsresource.h"
#include <SDL2/SDL_mixer.h>
#include <chrono>

/// @brief A resource object containing music.
class Music : public RWopsResource
{
private:
    Mix_Music *music;

public:
    Music(SDL_RWops *src);
    ~Music();

    /// @brief Returns the `Mix_Music` contained in this resource.
    Mix_Music *GetMusic() const;
    /// @brief Returns the duration of the music or 0 if it's invalid.
    std::chrono::milliseconds GetDuration() const;
    /// @brief Returns if the music in the resource is valid.
    bool IsValid() const override;
};

#endif