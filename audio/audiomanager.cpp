#include "audiomanager.h"
#include <SDL2/SDL.h>
#include <algorithm>

bool AudioManager::Start()
{
    if (SDL_InitSubSystem(SDL_INIT_AUDIO))
    {
        SDL_LogError(0, SDL_GetError());
        return false;
    }
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 2048))
    {
        SDL_LogError(0, Mix_GetError());
        return false;
    }
    Mix_ChannelFinished(OnChannelFinished);
    isRunning = true;
    fadeOutThread = std::thread(FadeOutLoop);
    fadeOutThread.detach();
    SDL_GetDefaultAudioInfo(nullptr, &audioSpec, 0);
    return true;
}

void AudioManager::OnChannelFinished(int channel)
{
    auto it = playing.find(channel);
    if (it != playing.end() && it->second)
    {
        it->second->OnFinished();
    }
}

AudioManager::FadeOutPoint AudioManager::RegisterFadeOut(int channel, int delayMs, int durationMs)
{
    auto end = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(delayMs);
    std::unique_lock<std::mutex> lck(fadeOutsMutex);
    auto it = fadeOuts.find(end);
    if (it != fadeOuts.end())
    {
        fadeOuts[end].push_back({channel, durationMs});
    }
    else
    {
        fadeOuts[end] = {{channel, durationMs}};
        fadeOutsModified = true;
    }
    lck.unlock();
    if (fadeOutsModified)
    {
        fadeOutCV.notify_all();
    }
    return end;
}

void AudioManager::UnregisterFadeOut(FadeOutPoint point, int channel)
{
    std::unique_lock<std::mutex> lck(fadeOutsMutex);
    auto it = fadeOuts.find(point);
    if (it != fadeOuts.end())
    {
        auto it2 = std::find_if(it->second.begin(), it->second.end(), [channel](FadeOut element)
        {
            return element.channel == channel;
        });
        if (it2 != it->second.end())
        {
            it->second.erase(it2);
        }
        if (it->second.size() == 0)
        {
            fadeOuts.erase(it);
            fadeOutsModified = true;
        }
    }
    lck.unlock();
    if (fadeOutsModified)
    {
        fadeOutCV.notify_all();
    }
}

void AudioManager::FadeOutLoop()
{
    std::unique_lock<std::mutex> fadeOutsLck(fadeOutsMutex, std::defer_lock);
    std::unique_lock<std::mutex> modifiedLck(modifiedMutex);
    while (true)
    {
        while (!fadeOuts.size() && isRunning)
        {
            fadeOutCV.wait(modifiedLck);
        }
        if (!isRunning)
        {
            return;
        }
        fadeOutsModified = false;
        fadeOutsLck.lock();
        auto point = fadeOuts.begin()->first;
        fadeOutsLck.unlock();
        fadeOutCV.wait_until(modifiedLck, point);
        if (!fadeOutsModified)
        {
            fadeOutsLck.lock();
            for (FadeOut channel : fadeOuts.begin()->second)
            {
                Mix_FadeOutChannel(channel.channel, channel.duration);
            }
            fadeOuts.erase(fadeOuts.begin());
            fadeOutsLck.unlock();
        }
    }
}

void AudioManager::Stop()
{
    isRunning = false;
    playing.clear();
    for (auto &music : AudioManager::music)
    {
        Mix_FreeMusic(music.second);
    }
    music.clear();
    std::unique_lock<std::mutex> lck(fadeOutsMutex);
    fadeOuts.clear();
    lck.unlock();
    fadeOutsModified = true;
    fadeOutCV.notify_all();
    Mix_CloseAudio();
    Mix_Quit();
    SDL_QuitSubSystem(SDL_INIT_AUDIO);
}

Mix_Music *AudioManager::GetMusic(const std::string &file)
{
    auto it = music.find(file);
    if (it != music.end())
    {
        return it->second;
    }
    Mix_Music *m = Mix_LoadMUS(file.c_str());
    if (!m)
    {
        SDL_LogError(0, "Failed to load music from file %s. %s.", file.c_str(), Mix_GetError());
    }
    music[file] = m;
    return m;
}

const SDL_AudioSpec &AudioManager::GetAudioSpec()
{
    return audioSpec;
}