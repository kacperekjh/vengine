#include "audio.h"
#include "audiomanager.h"

Audio::Audio(SDL_RWops *src) : RWopsResource(src), chunk(Mix_LoadWAV_RW(src, 0))
{
    if (!chunk)
    {
        SDL_LogError(0, "Failed to load audio chunk. %s.", Mix_GetError());
        duration = std::chrono::milliseconds(0);
    }
    else
    {
        const SDL_AudioSpec &spec = AudioManager::GetAudioSpec();
        float duration = chunk->alen / float(spec.freq * SDL_AUDIO_BITSIZE(spec.format) / 8 * spec.channels);
        Audio::duration = std::chrono::milliseconds((int64_t)std::ceil(duration * 1000));
    }
}

Audio::~Audio()
{
    if (chunk)
    {
        Mix_FreeChunk(chunk);
    }
}

Mix_Chunk *Audio::GetChunk() const
{
    return chunk;
}

std::chrono::milliseconds Audio::GetDuration() const
{
    return duration;
}

bool Audio::IsValid() const
{
    return chunk != nullptr;
}
