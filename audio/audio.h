#ifndef _VENGINE_AUDIO_AUDIO_
#define _VENGINE_AUDIO_AUDIO_

#include "../resources/rwopsresource.h"
#include <SDL2/SDL_mixer.h>
#include <chrono>

/// @brief A resource object containing a sound effect.
class Audio : public RWopsResource
{
private:
    Mix_Chunk *chunk;
    std::chrono::milliseconds duration;

public:
    Audio(SDL_RWops *src);
    ~Audio();

    /// @brief Returns the `Mix_Chunk` contained in this resource. 
    Mix_Chunk *GetChunk() const;
    /// @brief Returns the duration of the sound effect or 0 if it's invalid.
    std::chrono::milliseconds GetDuration() const;
    /// @brief Returns if the sound effect in the resource is valid.
    bool IsValid() const override;
};

#endif