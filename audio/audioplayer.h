#ifndef _VENGINE_AUDIO_AUDIOPLAYER_
#define _VENGINE_AUDIO_AUDIOPLAYER_

#include <chrono>
#include <optional>
#include "audio.h"
#include "../pointer.h"

/// @brief Class that allows to play sound effects.
class AudioPlayer
{
private:
    using FadeOutPoint = std::chrono::time_point<std::chrono::high_resolution_clock>;

    Pointer<Audio> audio;
    int channel = -1;
    unsigned char volume = MIX_MAX_VOLUME;
    int fadeInDuration = 0;
    int fadeOutDuration = 0;
    std::optional<FadeOutPoint> fadeOutPoint = std::nullopt;

    void Play(int loops, int timeLimit);
    void OnFinished();

public:
    /// @brief Constructs an audio object using `chunk` as its sound.
    AudioPlayer(Pointer<Audio> audio = nullptr);
    AudioPlayer(const AudioPlayer &b);
    ~AudioPlayer();
    /// @brief Changes volume of the sound effect.
    /// @param volume Value between 0.0f for silence to 1.0f for full volume. Values outside of the range are clamped to [0, 1].
    void SetVolume(float volume);
    /// @brief Returns value between 0.0f and 1.0f representing the current volume.
    float GetVolume() const;
    /// @brief Returns if the audio is currently playing.
    bool IsPlaying() const;
    /// @brief Plays the audio.
    /// @param loops Amount of loops to play. Negative values loop forever.
    /// @param timeLimit Max amount of time to play the audio for. Negative values mean no time limit.
    template <class Rep = int64_t, class Period = std::milli>
    void Play(int loops = 0, std::chrono::duration<Rep, Period> timeLimit = std::chrono::duration<Rep, Period>(-1));
    /// @brief Changes fade in duration of the audio.
    template <class Rep, class Period>
    void SetFadeInDuration(std::chrono::duration<Rep, Period> time);
    /// @brief Changes fade out duration of the audio.
    template <class Rep, class Period>
    void SetFadeOutDuration(std::chrono::duration<Rep, Period> time);
    /// @brief Stops playing the audio.
    void Stop();
    AudioPlayer &operator=(const AudioPlayer &b);
    /// @brief Returns the used audio resource.
    Pointer<const Audio> GetAudio() const;
    /// @brief Returns if this AudioPlayer has valid Audio resource assigned.
    bool HasAudio() const;

    friend class AudioManager;
};

#include "audioplayer.hxx"

#endif