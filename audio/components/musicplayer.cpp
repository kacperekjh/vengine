#include "musicplayer.h"

void MusicPlayer::StopMusic()
{
    if (isPlaying)
    {
        position = Mix_GetMusicPosition(music->GetMusic());
        Mix_HaltMusic();
    }
    else
    {
        wasPlaying = false;
    }
    isPlaying = false;
}

void MusicPlayer::RestartMusic()
{
    if (IsMain() && wasPlaying)
    {
        Resume();
    }
}

void MusicPlayer::Start()
{
    if (!GetScene()->GetInstance<MusicPlayer>())
    {
        MakeMain();
    }
}

void MusicPlayer::OnDestroy()
{
    StopMusic();
}

void MusicPlayer::OnSceneUnloaded()
{
    StopMusic();
}

void MusicPlayer::OnSceneLoaded()
{
    RestartMusic();
}

void MusicPlayer::SetVolume(float volume)
{
    MusicPlayer::volume = MIX_MAX_VOLUME * volume;
    if (isPlaying)
    {
        Mix_VolumeMusic(MusicPlayer::volume);
    }
}

float MusicPlayer::GetVolume() const
{
    return (float)volume / MIX_MAX_VOLUME;
}

void MusicPlayer::Pause()
{
    if (isPlaying)
    {
        StopMusic();
    }
}

void MusicPlayer::Resume()
{
    if (!IsMain())
    {
        SDL_LogError(0, "Cannot play music on a non-main music player.");
        return;
    }
    if (!isPlaying && music->IsValid())
    {
        Mix_PlayMusic(music->GetMusic(), -1);
        Mix_VolumeMusic(volume);
        Mix_SetMusicPosition(position);
        isPlaying = true;
    }
}

void MusicPlayer::SetMusic(const Pointer<Music> &music)
{
    MusicPlayer::music = music;
    if (isPlaying)
    {
        Mix_HaltMusic();
        if (music && music->IsValid())
        {
            Mix_PlayMusic(music->GetMusic(), -1);
        }
    }
}

bool MusicPlayer::IsPlaying() const
{
    return isPlaying;
}

bool MusicPlayer::IsMain() const
{
    return GetPointer() == GetScene()->GetInstance<MusicPlayer>();
}

void MusicPlayer::MakeMain()
{
    GetScene()->SetInstance(GetPointer().StaticCast<MusicPlayer>());
    RestartMusic();
}

std::chrono::milliseconds MusicPlayer::GetPosition() const
{
    if (isPlaying)
    {
        return std::chrono::milliseconds((int64_t)(Mix_GetMusicPosition(music->GetMusic()) * 1000));
    }
    else
    {
        return std::chrono::milliseconds((int64_t)(position * 1000));
    }
}