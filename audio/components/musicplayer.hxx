template <class Rep, class Period>
void MusicPlayer::SetPosition(std::chrono::duration<Rep, Period> position)
{
    std::chrono::milliseconds positionMs;
    if (music && music->IsValid())
    {
        positionMs = std::chrono::duration_cast<std::chrono::milliseconds>(position);
        positionMs = std::chrono::milliseconds(positionMs.count() % (music->GetDuration().count() + 1));
    }
    else
    {
        positionMs = std::chrono::milliseconds(0);
    }
    double pos = (double)positionMs.count();
    pos /= 1000;
    if (isPlaying)
    {
        Mix_SetMusicPosition(pos);
    }
    else
    {
        MusicPlayer::position = pos;
    }
}