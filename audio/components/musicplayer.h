#ifndef _VENGINE_AUDIO_MUSICPLAYER_
#define _VENGINE_AUDIO_MUSICPLAYER_

#include "../../gameobjects/component.h"
#include "../music.h"

/// @brief Component used to play music. Only one music player can be active on the a single scene.
class MusicPlayer : public Component
{
private:
    Pointer<Music> music = nullptr;
    unsigned char volume = MIX_MAX_VOLUME;
    double position = 0;
    bool wasPlaying = true;
    bool isPlaying = false;

    void StopMusic();
    void RestartMusic();

protected:
    void Start() override;
    void OnDestroy() override;
    void OnSceneUnloaded() override;
    void OnSceneLoaded() override;

public:
    /// @brief Changes volume of the music.
    /// @param volume Value between 0.0f for silence to 1.0f for full volume. Values outside of the range are clamped to [0, 1].
    void SetVolume(float volume);
    /// @brief Returns value between 0.0f and 1.0f representing the current volume.
    float GetVolume() const;
    /// @brief Stops playing the music.
    void Pause();
    /// @brief Starts/Resumes playing the music.
    void Resume();
    /// @brief Changes the used music resource.
    void SetMusic(const Pointer<Music> &music);
    /// @brief Returns if this music player is currently playing music.
    bool IsPlaying() const;
    /// @brief Returns if this music player is the main player of its scene.
    bool IsMain() const;
    /// @brief Sets this music player as the main player of its scene.
    void MakeMain();
    /// @brief Changes the position of the music.
    template <class Rep, class Period>
    void SetPosition(std::chrono::duration<Rep, Period> position);
    /// @brief Returns the position of the music.
    std::chrono::milliseconds GetPosition() const;
};

#include "musicplayer.hxx"

#endif