#include "music.h"

Music::Music(SDL_RWops *src) : RWopsResource(src), music(Mix_LoadMUS_RW(src, 0))
{
    if (!music)
    {
        SDL_LogError(0, "Failed to load music. %s.", Mix_GetError());
    }
}

Music::~Music()
{
    if (music)
    {
        Mix_FreeMusic(music);
    }
}

Mix_Music *Music::GetMusic() const
{
    return music;
}

std::chrono::milliseconds Music::GetDuration() const
{
    if (music)
    {
        return std::chrono::milliseconds((int64_t)(Mix_MusicDuration(music) * 1000));
    }
    return std::chrono::milliseconds(0);
}

bool Music::IsValid() const
{
    return music != nullptr;
}
