template <class Rep, class Period>
void AudioPlayer::Play(int loops, std::chrono::duration<Rep, Period> timeLimit)
{
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(timeLimit);
    if (ms.count() > std::numeric_limits<int>::max())
    {
        ms = std::chrono::milliseconds(std::numeric_limits<int>::max());
    }
    if (ms.count() < 0)
    {
        ms = std::chrono::milliseconds(-1);
    }
    Play(loops, ms.count());
}

template <class Rep, class Period>
void AudioPlayer::SetFadeInDuration(std::chrono::duration<Rep, Period> time)
{
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(time);
    if (ms.count() > std::numeric_limits<int>::max())
    {
        ms = std::chrono::milliseconds(std::numeric_limits<int>::max());
    }
    if (ms.count() < 0)
    {
        ms = std::chrono::milliseconds(0);
    }
    fadeInDuration = ms.count();
}

template <class Rep, class Period>
void AudioPlayer::SetFadeOutDuration(std::chrono::duration<Rep, Period> time)
{
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(time);
    if (ms.count() > std::numeric_limits<int>::max())
    {
        ms = std::chrono::milliseconds(std::numeric_limits<int>::max());
    }
    if (ms.count() < 0)
    {
        ms = std::chrono::milliseconds(0);
    }
    fadeOutDuration = ms.count();
}