#ifndef _VENGINE_AUDIO_AUDIOMANAGER_
#define _VENGINE_AUDIO_AUDIOMANAGER_

#include <chrono>
#include "audioplayer.h"
#include <map>
#include <string>
#include <thread>
#include <condition_variable>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_audio.h>

class AudioManager
{
private:
    struct FadeOut
    {
        int channel;
        int duration;
    };

    using FadeOutPoint = std::chrono::time_point<std::chrono::high_resolution_clock>;

    static inline std::atomic<bool> isRunning = false;
    static inline std::map<std::string, Mix_Music *> music = {};
    static inline std::map<int, AudioPlayer *> playing = {};
    static inline SDL_AudioSpec audioSpec;
    static inline std::thread fadeOutThread;
    static inline std::map<FadeOutPoint, std::vector<FadeOut>> fadeOuts = {};
    static inline std::mutex fadeOutsMutex;
    static inline std::condition_variable fadeOutCV;
    static inline std::unique_lock<std::mutex> fadeOutLock;
    static inline std::atomic<bool> fadeOutsModified = false;
    static inline std::mutex modifiedMutex;

    static bool Start();
    static void Stop();
    static void OnChannelFinished(int channel);
    static FadeOutPoint RegisterFadeOut(int channel, int delayMs, int durationMs);
    static void UnregisterFadeOut(FadeOutPoint point, int channel);
    static void FadeOutLoop();

public:
    /// @brief Opens a music file.
    static Mix_Music *GetMusic(const std::string &file);
    /// @brief Returns the current SDL_AudioSpec.
    static const SDL_AudioSpec &GetAudioSpec();

    friend class GameLoop;
    friend class AudioPlayer;
};

#endif