#include "audioplayer.h"
#include <SDL2/SDL.h>
#include <limits>
#include "audiomanager.h"

void AudioPlayer::Play(int loops, int timeLimit)
{
    if (!HasAudio())
    {
        SDL_LogWarn(0, "Failed to play audio. Missing audio resource.");
        return;
    }
    if (loops < 0)
    {
        loops = -1;
    }
    channel = Mix_FadeInChannelTimed(channel, audio->GetChunk(), loops, fadeInDuration, timeLimit);
    if (channel == -1)
    {
        SDL_LogError(0, "Failed to play audio.");
        return;
    }
    Mix_Volume(channel, volume);
    AudioManager::playing[channel] = this;
    if (fadeOutDuration > 0)
    {
        fadeOutPoint = AudioManager::RegisterFadeOut(channel, audio->GetDuration().count() - fadeOutDuration, fadeOutDuration);
    }
}

void AudioPlayer::OnFinished()
{
    channel = -1;
    fadeOutPoint = std::nullopt;
}

AudioPlayer::AudioPlayer(Pointer<Audio> audio) : audio(audio) {}

AudioPlayer::AudioPlayer(const AudioPlayer &b) : audio(b.audio), volume(b.volume), fadeInDuration(b.fadeInDuration), fadeOutDuration(b.fadeOutDuration) {}

AudioPlayer::~AudioPlayer()
{
    if (IsPlaying())
    {
        AudioManager::playing[channel] = nullptr;
    }
}

void AudioPlayer::SetVolume(float volume)
{
    AudioPlayer::volume = volume * MIX_MAX_VOLUME;
    if (IsPlaying())
    {
        Mix_Volume(channel, AudioPlayer::volume);
    }
}

float AudioPlayer::GetVolume() const
{
    return (float)volume / MIX_MAX_VOLUME;
}

bool AudioPlayer::IsPlaying() const
{
    return channel != -1;
}

void AudioPlayer::Stop()
{
    if (channel != -1)
    {
        if (fadeOutPoint)
        {
            AudioManager::UnregisterFadeOut(fadeOutPoint.value(), channel);
        }
        if (Mix_HaltChannel(channel))
        {
            SDL_LogError(0, "Failed to stop audio. %s", Mix_GetError());
        }
    }
    else
    {
        SDL_LogWarn(0, "Failed to stop audio. Not playing.");
    }
    channel = -1;
}

AudioPlayer &AudioPlayer::operator=(const AudioPlayer &b)
{
    if (&b != this)
    {
        if (IsPlaying())
        {
            AudioManager::playing[channel] = nullptr;
        }
        audio = b.audio;
        channel = -1;
        volume = b.volume;
        fadeInDuration = b.fadeInDuration;
        fadeOutDuration = b.fadeOutDuration;
        fadeOutPoint = std::nullopt;
    }
    return *this;
}

Pointer<const Audio> AudioPlayer::GetAudio() const
{
    return audio;
}

bool AudioPlayer::HasAudio() const
{
    return audio && audio->IsValid();
}