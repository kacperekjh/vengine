#ifndef _VENGINE_STRUCTS_LINE3D_
#define _VENGINE_STRUCTS_LINE3D_

#include "vector3.h"
#include <vector>

/// @brief Struct representing a line made out of connected points.
class Line3D
{
private:
    std::vector<Vector3> points;

public:
    /// Creates a new empty line.
    Line3D();
    /// @brief Creates a new line made out of connected `points`.
    Line3D(std::vector<Vector3> points);
    /// @brief Returns points, that this line is made out of.
    const std::vector<Vector3> &GetPoints() const;
    /// @brief Sets new points of the line.
    void SetPoints(const std::vector<Vector3> &points);
    /// @brief Moves all points of the line by `offset`.
    Line3D Translate(const Vector3 &offset) const;
    /// @brief Rotates all points of this line around `pivot`.
    /// @param rad rotation vector which's axies are interpreted as angles in radians.
    Line3D Rotate(const Vector3 &rad, const Vector3 &pivot = {}) const;
    /// @brief Scales all points by `scale` relative to `pivot`.
    Line3D Scale(const Vector3 &scale, const Vector3 &pivot = {}) const;
};

#endif