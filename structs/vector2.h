#ifndef _VENGINE_STRUCTS_VECTOR2_
#define _VENGINE_STRUCTS_VECTOR2_

struct Vector3;
struct Vector2Int;

/// @brief Struct representing a two dimensional vector.
struct Vector2
{
    float x;
    float y;

    /// @brief Constructs an empty [0, 0] vector.
    Vector2();
    /// @brief Constrict a vector with coordinates [x, y].
    Vector2(float x, float y);
    /// @brief Calculates the sum of this vector and vector `b`.
    Vector2 operator+(const Vector2 &b) const;
    /// @brief Calculates the difference of this vector and vector `b`.
    Vector2 operator-(const Vector2 &b) const;
    /// @brief Calculates the result of multiplication of this vector and scalar `v`.
    Vector2 operator*(float v) const;
    /// @brief Calculates the result of division of this vector by scalar `v`.
    Vector2 operator/(float v) const;
    /// @brief Negates the vector
    Vector2 operator-() const;
    /// @brief Calculates the sum of this vector and vector `b`.
    Vector2 &operator+=(const Vector2 &b);
    /// @brief Calculates the difference of this vector and vector `b`.
    Vector2 &operator-=(const Vector2 &b);
    /// @brief Multiplies this vector by scalar `v`.
    Vector2 &operator*=(float v);
    /// @brief Divides this vector by scalar `v`.
    Vector2 &operator/=(float v);
    /// @brief Compares if this vector is equal to vector `b`.
    bool operator==(const Vector2 &b) const;
    /// @brief Compares if this vector is not equal to vector `b`.
    bool operator!=(const Vector2 &b) const;
    /// @brief Calculates the length/magnitude of this vector.
    float GetMagnitude() const;
    /// @brief Calculated the squared length/magnitude of this vector.
    float GetSquareMagnitude() const;
    /// @brief Calculates an angle in radians based on the direction of this vector. Where [0, 1] represents 0deg going counterclockwise.
    float ToAngle() const;
    /// @brief Linearly interpolates this vector with vector `b` by `t` interpolation factor.
    Vector2 Lerp(const Vector2 &b, float t) const;
    /// @brief Calculates a normalized vector (that has length equal 1) from this vector.
    Vector2 Normalized() const;
    /// @brief Creates a new directional vector from `angle` in radians.
    /// @see Vector2::ToAngle()
    static Vector2 FromAngle(float angle);
    /// @brief Normalizes this vector so it has length equal 1.
    void Normalize();
    /// @brief Converts this vector into a Vector3.
    operator Vector3() const;
    /// @brief Converts this vector into a Vector3Int.
    explicit operator Vector2Int() const;
    /// @brief Rotates this vector by `rad` radians.
    Vector2 Rotate(float rad) const;
    /// @brief Scales this vector by vector `scale`.
    Vector2 Scale(const Vector2 &scale) const;
    /// @brief Returns the directional vector from `a` to `b`.
    static Vector2 Direction(Vector2 a, Vector2 b);
};

#endif