#ifndef _VENGINE_STRUCTS_VECTOR3_
#define _VENGINE_STRUCTS_VECTOR3_

struct Vector2;

/// @brief Struct representing a three dimensional vector.
struct Vector3
{
    float x;
    float y;
    float z;

    /// @brief Constructs an empty [0, 0, 0] vector.
    Vector3();
    /// @brief Constrict a vector with coordinates [x, y, z].
    Vector3(float x, float y, float z);
    /// @brief Calculates the sum of this vector and vector `b`.
    Vector3 operator+(const Vector3 &b) const;
    /// @brief Calculates the difference of this vector and vector `b`.
    Vector3 operator-(const Vector3 &b) const;
    /// @brief Calculates the result of multiplication of this vector and scalar `v`.
    Vector3 operator*(float v) const;
    /// @brief Calculates the result of division of this vector by scalar `v`.
    Vector3 operator/(float v) const;
    /// @brief Negates the vector
    Vector3 operator-() const;
    /// @brief Calculates the sum of this vector and vector `b`.
    Vector3 &operator+=(const Vector3 &b);
    /// @brief Calculates the difference of this vector and vector `b`.
    Vector3 &operator-=(const Vector3 &b);
    /// @brief Multiplies this vector by scalar `v`.
    Vector3 &operator*=(float v);
    /// @brief Divides this vector by scalar `v`.
    Vector3 &operator/=(float v);
    /// @brief Compares if this vector is equal to vector `b`.
    bool operator==(const Vector3 &b) const;
    /// @brief Compares if this vector is not equal to vector `b`.
    bool operator!=(const Vector3 &b) const;
    /// @brief Calculates the length/magnitude of this vector.
    float GetMagnitude() const;
    /// @brief Calculated the squared length/magnitude of this vector.
    float GetSquareMagnitude() const;
    /// @brief Linearly interpolates this vector with vector `b` by `t` interpolation factor.
    Vector3 Lerp(const Vector3 &b, float t) const;
    /// @brief Calculates a normalized vector (that has length equal 1) from this vector.
    Vector3 Normalized() const;
    /// @brief Normalizes this vector so it has length equal 1.
    void Normalize();
    /// @brief Converts this vector into a Vector2, discarding z cooridinate.
    explicit operator Vector2() const;
    /// @brief Rotates this vector by `rad`, which's axies are interpreted as angles in radians.
    Vector3 Rotate(const Vector3 &rad) const;
    /// @brief Scales this vector by vector `scale`.
    Vector3 Scale(const Vector3 &scale) const;
    /// @brief Returns the directional vector from `a` to `b`.
    static Vector3 Direction(const Vector3 &a, const Vector3 &b);
};

#endif