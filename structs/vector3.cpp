#include "vector3.h"
#include "vector2.h"
#include "../math.h"

Vector3::Vector3() : x(0), y(0), z(0) {}

Vector3::Vector3(float x, float y, float z) : x(x), y(y), z(z) {}

Vector3 Vector3::operator+(const Vector3 &b) const
{
    return Vector3(x + b.x, y + b.y, z + b.z);
}

Vector3 Vector3::operator-(const Vector3 &b) const
{
    return Vector3(x - b.x, y - b.y, z - b.z);
}

Vector3 Vector3::operator*(float v) const
{
    return Vector3(x * v, y * v, z * v);
}

Vector3 Vector3::operator/(float v) const
{
    return Vector3(x / v, y / v, z / v);
}

Vector3 &Vector3::operator+=(const Vector3 &b)
{
    x += b.x;
    y += b.y;
    z += b.z;
    return *this;
}

Vector3 &Vector3::operator-=(const Vector3 &b)
{
    x -= b.x;
    y -= b.y;
    z -= b.z;
    return *this;
}

Vector3 &Vector3::operator*=(float v)
{
    x *= v;
    y *= v;
    z *= v;
    return *this;
}

Vector3 &Vector3::operator/=(float v)
{
    x /= v;
    y /= v;
    z /= v;
    return *this;
}

Vector3 Vector3::operator-() const
{
    return Vector3(-x, -y, -z);
}

bool Vector3::operator==(const Vector3 &b) const
{
    return x == b.x && y == b.y && z == b.z;
}

bool Vector3::operator!=(const Vector3 &b) const
{
    return x != b.x || y != b.y || z != b.z;
}

float Vector3::GetMagnitude() const
{
    return sqrt(x * x + y * y + z * z);
}

float Vector3::GetSquareMagnitude() const
{
    return x * x + y * y + z * z;
}

Vector3 Vector3::Lerp(const Vector3 &b, float t) const
{
    return Vector3(::Lerp(x, b.x, t), ::Lerp(y, b.y, t), ::Lerp(z, b.z, t));
}

Vector3 Vector3::Normalized() const
{
    float mag = GetMagnitude();
    if (mag <= __FLT_EPSILON__)
    {
        return Vector3();
    }
    return Vector3(x / mag, y / mag, z / mag);
}

void Vector3::Normalize()
{
    float mag = GetMagnitude();
    if (mag > __FLT_EPSILON__)
    {
        x /= mag;
        y /= mag;
        z /= mag;
    }
}

Vector3::operator Vector2() const
{
    return Vector2(x, y);
}

Vector3 Vector3::Rotate(const Vector3 &rad) const
{
    float sinv, cosv;
    Vector3 newPoint;

    // x
    sinv = sin(rad.x);
    cosv = cos(rad.x);
    newPoint = Vector3(x, cosv * y - sinv * z, sinv * y + cosv * z);
    // y
    sinv = sin(rad.y);
    cosv = cos(rad.y);
    newPoint = Vector3(cosv * newPoint.x + sinv * newPoint.z, newPoint.y, -sinv * newPoint.x + cosv * newPoint.z);
    // z
    sinv = sin(rad.z);
    cosv = cos(rad.z);
    newPoint = Vector3(cosv * newPoint.x - sinv * newPoint.y, sinv * newPoint.x + cosv * newPoint.y, newPoint.z);

    return newPoint;
}

Vector3 Vector3::Scale(const Vector3 &scale) const
{
    return {x * scale.x, y * scale.y, z * scale.z};
}

Vector3 Vector3::Direction(const Vector3 &a, const Vector3 &b)
{
    return (b - a).Normalized();
}