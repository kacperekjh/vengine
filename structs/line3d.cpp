#include "line3d.h"
#include <cmath>

Line3D::Line3D()
{
    SetPoints({});
}

Line3D::Line3D(std::vector<Vector3> points) 
{
    SetPoints(points);
}

const std::vector<Vector3> &Line3D::GetPoints() const
{
    return points;
}

void Line3D::SetPoints(const std::vector<Vector3> &points)
{
    Line3D::points = points;
}

Line3D Line3D::Translate(const Vector3 &offset) const
{
    std::vector<Vector3> points;
    points.reserve(Line3D::points.size());
    for (const auto &point : Line3D::points)
    {
        points.push_back(point + offset);
    }
    return Line3D(points);
}

Line3D Line3D::Rotate(const Vector3 &rad, const Vector3 &pivot) const
{
    std::vector<Vector3> points;
    points.reserve(Line3D::points.size());
    for (const auto &point : Line3D::points)
    {
        points.push_back((point - pivot).Rotate(rad) + pivot);
    }
    return Line3D(points);
}

Line3D Line3D::Scale(const Vector3 &scale, const Vector3 &pivot) const
{
    std::vector<Vector3> points;
    points.reserve(Line3D::points.size());
    for (const auto &point : Line3D::points)
    {
        points.push_back((point - pivot).Scale(scale) + pivot);
    }
    return Line3D(points);
}