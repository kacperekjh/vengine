#ifndef _VENGINE_STRUCTS_COLOR_
#define _VENGINE_STRUCTS_COLOR_

#include <cstdint>

/// @brief Struct representing an RGBA color, encoded using 4 floats.
struct Color
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;

    /// @brief Constructs the color from binary encoded RGBA data.
    Color(uint32_t color);
    /// @brief Constructs the color from 4 color channels.
    Color(uint8_t r = 0, uint8_t g = 0, uint8_t b = 0, uint8_t a = 255);
    /// @brief Linearly interpolates this color with color `b` by `t` interpolation factor.
    Color Lerp(const Color &b, float t) const;
    /// @brief Blends this color with color `b` using normal blend mode.
    Color operator+(const Color &b) const;
    /// @brief Converts this color to binary encoded RGBA data.
    operator uint32_t() const;
    /// @brief Returns the default color for invalid or missing textures.
    static Color InvalidColor();
};

#endif