#include "vector2.h"
#include "vector3.h"
#include "vector2int.h"
#include "../math.h"

Vector2::Vector2() : x(0), y(0) {}

Vector2::Vector2(float x, float y) : x(x), y(y) {}

Vector2 Vector2::operator+(const Vector2 &b) const
{
    return Vector2(x + b.x, y + b.y);
}

Vector2 Vector2::operator-(const Vector2 &b) const
{
    return Vector2(x - b.x, y - b.y);
}

Vector2 Vector2::operator*(float v) const
{
    return Vector2(x * v, y * v);
}

Vector2 Vector2::operator/(float v) const
{
    return Vector2(x / v, y / v);
}

Vector2 Vector2::operator-() const
{
    return Vector2(-x, -y);
}

Vector2 &Vector2::operator+=(const Vector2 &b)
{
    x += b.x;
    y += b.y;
    return *this;
}

Vector2 &Vector2::operator-=(const Vector2 &b)
{
    x -= b.x;
    y -= b.y;
    return *this;
}

Vector2 &Vector2::operator*=(float v)
{
    x *= v;
    y *= v;
    return *this;
}

Vector2 &Vector2::operator/=(float v)
{
    x /= v;
    y /= v;
    return *this;
}

bool Vector2::operator==(const Vector2 &b) const
{
    return x == b.x && y == b.y;
}

bool Vector2::operator!=(const Vector2 &b) const
{
    return x != b.x || y != b.y;
}

float Vector2::GetMagnitude() const
{
    return sqrt(x * x + y * y);
}

float Vector2::GetSquareMagnitude() const
{
    return x * x + y * y;
}

float Vector2::ToAngle() const
{
    return atan2(y, x);
}

Vector2 Vector2::Lerp(const Vector2 &b, float t) const
{
    return Vector2(::Lerp(x, b.x, t), ::Lerp(y, b.y, t));
}

Vector2 Vector2::Normalized() const
{
    float mag = GetMagnitude();
    if (mag <= __FLT_EPSILON__)
    {
        return Vector2();
    }
    return Vector2(x / mag, y / mag);
}

Vector2 Vector2::FromAngle(float angle)
{
    return Vector2(cosf(angle), sinf(angle));
}

void Vector2::Normalize()
{
    float mag = GetMagnitude();
    if (mag > __FLT_EPSILON__)
    {
        x /= mag;
        y /= mag;
    }
}

Vector2::operator Vector3() const
{
    return Vector3(x, y, 0);
}

Vector2::operator Vector2Int() const
{
    return Vector2Int((int)x, (int)y);
}

Vector2 Vector2::Rotate(float rad) const
{
    float sinv = sin(rad);
    float cosv = cos(rad);
    return Vector2(x * cosv - y * sinv, x * sinv + y * cosv);
}

Vector2 Vector2::Scale(const Vector2 &scale) const
{
    return {x * scale.x, y * scale.y};
}

Vector2 Vector2::Direction(Vector2 a, Vector2 b)
{
    return (b - a).Normalized();
}