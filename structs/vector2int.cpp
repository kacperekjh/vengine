#include "vector2int.h"
#include "vector2.h"

Vector2Int::Vector2Int() : x(0), y(0) {}

Vector2Int::Vector2Int(int x, int y) : x(x), y(y) {}

Vector2Int Vector2Int::operator+(const Vector2Int &b) const
{
    return Vector2Int(x + b.x, y + b.y);
}

Vector2Int Vector2Int::operator-(const Vector2Int &b) const
{
    return Vector2Int(x - b.x, y - b.y);
}

Vector2Int Vector2Int::operator*(int v) const
{
    return Vector2Int(x * v, y * v);
}

Vector2Int Vector2Int::operator/(int v) const
{
    return Vector2Int(x / v, y / v);
}

Vector2Int Vector2Int::operator-() const
{
    return Vector2Int(-x, -y);
}

Vector2Int &Vector2Int::operator+=(const Vector2Int &b)
{
    x += b.x;
    y += b.y;
    return *this;
}

Vector2Int &Vector2Int::operator-=(const Vector2Int &b)
{
    x -= b.x;
    y -= b.y;
    return *this;
}

Vector2Int &Vector2Int::operator*=(int v)
{
    x *= v;
    y *= v;
    return *this;
}

Vector2Int &Vector2Int::operator/=(int v)
{
    x /= v;
    y /= v;
    return *this;
}

bool Vector2Int::operator==(const Vector2Int &b) const
{
    return x == b.x && y == b.y;
}

bool Vector2Int::operator!=(const Vector2Int &b) const
{
    return x != b.x || y != b.y;
}

Vector2Int::operator Vector2() const
{
    return Vector2(x, y);
}