#include "color.h"
#include "../math.h"

Color::Color(uint32_t color) : r(color & 0x000000FF), g(color >> 8 & 0x000000FF), b(color >> 16 & 0x000000FF), a(color >> 24 & 0x000000FF) {}

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a) : r(r), g(g), b(b), a(a) {}

Color Color::Lerp(const Color &b, float t) const
{
    return Color(r * (1 - t) + b.r * t, g * (1 - t) + b.g * t, Color::b * (1 - t) + b.b * t, a * (1 - t) + b.a * t);
}

Color Color::operator+(const Color &b) const
{
    float t = b.a / 255.f;
    uint16_t a = Color::a + b.a;
    a = a > 255 ? 255 : a;
    return Color(r * (1 - t) + b.r * t, g * (1 - t) + b.g * t, Color::b * (1 - t) + b.b * t, a);
}

Color::operator uint32_t() const
{
    uint32_t color = 0;
    color |= r;
    color |= g << 8;
    color |= b << 16;
    color |= a << 24;
    return color;
}

Color Color::InvalidColor()
{
    return Color(255, 0, 255);
}