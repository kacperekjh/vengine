#ifndef _VENGINE_STRUCTS_VECTOR2INT_
#define _VENGINE_STRUCTS_VECTOR2INT_

struct Vector2;

/// @brief Struct representing a two dimensional vector with intergral cooridnates.
struct Vector2Int
{
    int x;
    int y;

    /// @brief Constructs an empty [0, 0] vector.
    Vector2Int();
    /// @brief Constrict a vector with coordinates [x, y].
    Vector2Int(int x, int y);
    /// @brief Calculates the sum of this vector and vector `b`.
    Vector2Int operator+(const Vector2Int &b) const;
    /// @brief Calculates the difference of this vector and vector `b`.
    Vector2Int operator-(const Vector2Int &b) const;
    /// @brief Calculates the result of multiplication of this vector and scalar `v`.
    Vector2Int operator*(int v) const;
    /// @brief Calculates the result of division of this vector by scalar `v`.
    Vector2Int operator/(int v) const;
    /// @brief Negates the vector
    Vector2Int operator-() const;
    /// @brief Calculates the sum of this vector and vector `b`.
    Vector2Int &operator+=(const Vector2Int &b);
    /// @brief Calculates the difference of this vector and vector `b`.
    Vector2Int &operator-=(const Vector2Int &b);
    /// @brief Multiplies this vector by scalar `v`.
    Vector2Int &operator*=(int v);
    /// @brief Divides this vector by scalar `v`.
    Vector2Int &operator/=(int v);
    /// @brief Compares if this vector is equal to vector `b`.
    bool operator==(const Vector2Int &b) const;
    /// @brief Compares if this vector is not equal to vector `b`.
    bool operator!=(const Vector2Int &b) const;
    /// @brief Converts this vector into a Vector2.
    operator Vector2() const;
};

#endif