#ifndef _VENGINE_UI_UIINPUT_
#define _VENGINE_UI_UIINPUT_

#include "uielement.h"

/// @brief Base class for all input UI elements.
class UIInput : public virtual UIElement
{
private:
    inline static Pointer<UIInput> focused;

protected:
    /// @brief Function called when the element is focused.
    virtual void OnFocus() = 0;
    /// @brief Function called when the element loses focus.
    virtual void OnUnfocus() = 0;
    void OnDisable() override;

public:
    virtual ~UIInput();
    /// @brief Focuses on this element.
    void Focus();
    /// @brief Checks if this input is focused.
    bool IsFocused() const;
    /// @brief Returns the focused input.
    static const Pointer<UIInput> &GetFocus();
    /// @brief Changes the focused element. Can be null.
    static void SetFocus(const Pointer<UIInput> element);
};

#endif