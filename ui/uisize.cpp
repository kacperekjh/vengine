#include "uisize.h"

UISize::UISize(const UISizeExpression &x, const UISizeExpression &y) : x(x), y(y) {}

Vector2Int UISize::Evaluate(Vector2Int containerSize, Vector2Int preferredSize) const
{
    return {x.Evaluate(containerSize.x, preferredSize.x), y.Evaluate(containerSize.y, preferredSize.y)};
}