#ifndef _VENGINE_UI_FONT_
#define _VENGINE_UI_FONT_

#include "../resources/rwopsresource.h"
#include <SDL2/SDL_ttf.h>
#include <map>

/// @brief A resource object containing a sound effect.
class Font : public RWopsResource
{
private:
    mutable std::map<int, TTF_Font *> fonts = {};
    TTF_Font *base;

public:
    Font(SDL_RWops *src);
    ~Font();
    /// @brief Returns if the font in the resource is valid.
    bool IsValid() const override;
    /// @brief Returns a `TTF_Font` of specified size. Results are cached for later.
    /// @param ptSize 
    /// @return 
    TTF_Font *GetFont(int ptSize) const;
};

#endif