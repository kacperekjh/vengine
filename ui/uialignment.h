#ifndef _VENGINE_UI_UIALIGNMENT_
#define _VENGINE_UI_UIALIGNMENT_

/// @brief Enum representing alignment of elements.
///        In horizontal layout direction, `Begin` is the left side and `End` is the right side.
///        In vertical layouts, `Begin` is top and `End` is bottom.
enum class UIAlignment
{
    Begin,
    Center,
    End
};

/// @brief Aligns element inside a container.
/// @param alignment alignment of the element
/// @param size size of the element
/// @param contianerSize size of the container
/// @return offset position
int UIAlign(UIAlignment alignment, int size, int contianerSize);

#endif