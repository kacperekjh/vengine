#include "uidimension.h"
#include <stdexcept>

int UIDimension::Evaluate(int containerSize, int preferred) const
{
    switch (unit)
    {
    case UIUnit::Pixel:
        return size;
    case UIUnit::Percent:
        return size * containerSize;
    case UIUnit::Auto:
        return preferred;        
    }
    throw std::runtime_error("Invalid unit.");
    return unit == UIUnit::Percent ? size * containerSize : size;
}