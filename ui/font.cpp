#include "font.h"

Font::Font(SDL_RWops *src) : RWopsResource(src), base(TTF_OpenFontRW(src, 0, 0))
{
    if (!base)
    {
        SDL_LogError(0, "Failed to load font from. %s.", TTF_GetError());
    }
}

Font::~Font()
{
    if (base)
    {
        TTF_CloseFont(base);
    }
    for (auto font : fonts)
    {
        TTF_CloseFont(font.second);
    }
}

bool Font::IsValid() const
{
    return base != nullptr;
}

TTF_Font *Font::GetFont(int ptSize) const
{
    if (!base)
    {
        return nullptr;
    }
    auto it = fonts.find(ptSize);
    if (it != fonts.end())
    {
        return it->second;
    }
    SDL_RWseek(src, 0, RW_SEEK_SET);
    TTF_Font *font = TTF_OpenFontRW(src, 0, ptSize);
    fonts[ptSize] = font;
    return font;
}