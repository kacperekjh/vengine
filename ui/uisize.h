#ifndef _VENGINE_UI_UISIZE_
#define _VENGINE_UI_UISIZE_

#include "../structs/vector2int.h"
#include "uisizeexpression.h"

struct UISize
{
    UISizeExpression x;
    UISizeExpression y;

    UISize(const UISizeExpression &x = 0_px, const UISizeExpression &y = 0_px);
    /// @brief Evaluates the size in pixels inside a container with dimensions `containerSize`.
    Vector2Int Evaluate(Vector2Int containerSize, Vector2Int preferredSize) const;
};

#endif