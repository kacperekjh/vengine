#include "uiinput.h"

void UIInput::OnDisable()
{
    if (IsFocused())
    {
        SetFocus(nullptr);
    }
}

UIInput::~UIInput()
{
    if (IsFocused())
    {
        focused = nullptr;
    }
}

void UIInput::Focus()
{
    SetFocus(GetPointer().DynamicCast<UIInput>());
}

bool UIInput::IsFocused() const
{
    return GetPointer().DynamicCast<const UIInput>() == focused;
}

const Pointer<UIInput> &UIInput::GetFocus()
{
    return focused;
}

void UIInput::SetFocus(const Pointer<UIInput> element)
{
    if (focused != element)
    {
        if (focused)
        {
            focused->OnUnfocus();
        }
        if (element)
        {
            element->OnFocus();
        }
        focused = element;
    }
}