#include "uialignment.h"
#include <stdexcept>

int UIAlign(UIAlignment alignment, int size, int contianerSize)
{
    switch (alignment)
    {
    case UIAlignment::Begin:
        return 0;
    case UIAlignment::Center:
        return (contianerSize - size) / 2;
    case UIAlignment::End:
        return contianerSize - size;
    }
    throw std::runtime_error("Invalid alignment value.");
}