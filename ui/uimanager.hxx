#include <stdexcept>

template <ValidUIElement T>
Pointer<T> UIManager::CreateElement(const Pointer<UIElement> &parent)
{
    UIElement *element = new T();
    element->parent = parent;
    if (parent)
    {
        parent->children.push_back(element->GetPointer());
        parent->OnAddChild(element->GetPointer());
        element->isOnScene = parent->isOnScene;
    }
    if (element->isOnScene)
    {
        element->OnEnable();
    }
    return element->GetPointer().DynamicCast<T>();
}