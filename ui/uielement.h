#ifndef _VENGEINE_UI_UIELEMENT_
#define _VENGEINE_UI_UIELEMENT_

#include <SDL2/SDL.h>
#include "uisize.h"
#include "uimargin.h"
#include <vector>
#include "../mixins/sharedpointer.h"
#include "../rendering/material.h"

/// @brief Base class for all UI elements.
class UIElement : public SharedPointer<UIElement>
{
private:
    Pointer<UIElement> parent = nullptr;
    std::vector<Pointer<UIElement>> children;
    UISize size = {};
    Vector2Int pixelSize = {};
    UIMargin margin = {};
    UIMarginPX pixelMargin = {};
    bool isOnScene = true;

    void UpdateSize(bool updateParents, bool updateChildren);
    void SetOnScene(bool state);

protected:
    /// @brief Returns size in pixel of the parent element or resolution of the window it it's null.
    Vector2Int GetParentSize() const;
    /// @brief Constructs a new UI element.
    UIElement();
    /// @brief Function called when a new child element is added.
    virtual void OnAddChild(const Pointer<UIElement> &child);
    /// @brief Function called when a child element is removed.
    virtual void OnRemoveChild(const Pointer<UIElement> &child);
    /// @brief Function called when this element is enabled or when it's created and is active.
    virtual void OnEnable();
    /// @brief Function called when this element is disabled.
    virtual void OnDisable();
    /// @brief Function called when the size of this element is changed.
    virtual void OnSizeChange();
    /// @brief Changes the size of this element.
    void SetSize(UISize size);
    /// @brief Recalculates the size and margins of this element. Redraws it if any changes occur.
    void UpdateSize();
    /// @brief Caclulates the preferred size of this element.
    virtual Vector2Int GetPreferredSize() const;
    /// @brief Calculates the maximum space that children can occupy.
    virtual Vector2Int GetAvailableSpace() const;
public:
    virtual ~UIElement();
    /// @brief Returns the size of this element.
    UISize GetSize() const;
    /// @brief Returns the size in pixels of this element.
    Vector2Int GetPixelSize() const;
    /// @brief Returns the margin of this element.
    UIMargin GetMargin() const;
    /// @brief Returns the margin of this element in pixels.
    UIMarginPX GetPixelMargin() const;
    /// @brief Changes the margin of this element.
    void SetMargin(UIMargin margin);
    /// @brief Returns pointer to the parent of this element.
    const Pointer<UIElement> &GetParent() const;
    /// @brief Returns a vector of this element's children.
    const std::vector<Pointer<UIElement>> GetChildren() const;
    /// @brief Returns if this element is enabled.
    bool GetState() const;
    /// @brief Function called when to draw the UI element. Before this function is called,
    ///        VBO and IBO are already set up.
    virtual void Draw(Vector2Int position) = 0;

    friend class UIManager;
    friend class Scene;
};

#endif