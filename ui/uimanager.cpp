#include "uimanager.h"
#include "fontmanager.h"
#include "../rendering/renderer.h"
#include "../gameobjects/objectmanager.h"

bool UIManager::Start()
{
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo);
    glGenVertexArrays(1, &vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indecies), indecies, GL_STATIC_DRAW);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void *)sizeof(Vector3));
    glBindVertexArray(0);
    return FontManager::Start();
}

void UIManager::Stop()
{
    FontManager::Stop();
    glDeleteBuffers(1, &ibo);
    glDeleteBuffers(1, &vbo);
}

void UIManager::Draw()
{
    auto scene = ObjectManager::GetCurrentScene();
    if (scene)
    {
        auto root = scene->uiRoot;
        if (root)
        {
            glBindVertexArray(vao);
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
            root->Draw({0, 0});
            glBindVertexArray(0);
        }
    }
}

void UIManager::CreateQuad(Vector2Int position, Vector2Int size)
{
    Vector3 pos = (Vector2)position;
    vertices[0].position = pos;
    vertices[1].position = pos + Vector3(0, size.y, 0);
    vertices[2].position = pos + Vector3(size.x, 0, 0);
    vertices[3].position = pos + Vector3(size.x, size.y, 0);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indecies), indecies, GL_STATIC_DRAW);
}