#include <algorithm>
#include "uielement.h"
#include "../rendering/renderer.h"
#include "uimanager.h"

void UIElement::UpdateSize(bool updateParents, bool updateChildren)
{
    Vector2Int parentSize = GetParentSize();
    Vector2Int size = UIElement::size.Evaluate(parentSize, GetPreferredSize());
    UIMarginPX margin = UIElement::margin.Evaluate(parentSize, {});
    if (pixelSize != size || margin != pixelMargin)
    {
        pixelSize = size;
        pixelMargin = margin;
        if (updateParents && parent)
        {
            parent->UpdateSize(true, false);
        }
        if (updateChildren)
        {
            for (auto &child : children)
            {
                child->UpdateSize(false, true);
            }
        }
    }
}

void UIElement::SetOnScene(bool state)
{
    if (state ^ isOnScene)
    {
        isOnScene = state;
        if (state)
        {
            OnEnable();
        }
        else
        {
            OnDisable();
        }
        for (auto &child : children)
        {
            child->SetOnScene(state);
        }
    }
}

Vector2Int UIElement::GetParentSize() const
{
    return parent ? parent->GetAvailableSpace() : Renderer::GetResolution();
}

UIElement::UIElement() {}

void UIElement::OnAddChild(const Pointer<UIElement> &child) {}

void UIElement::OnRemoveChild(const Pointer<UIElement> &child) {}

void UIElement::OnEnable() {}

void UIElement::OnDisable() {}

void UIElement::OnSizeChange() {}

void UIElement::SetSize(UISize size)
{
    UIElement::size = size;
    UpdateSize();
}

void UIElement::UpdateSize()
{
    UpdateSize(true, true);
}

Vector2Int UIElement::GetPreferredSize() const
{
    return {};
}

Vector2Int UIElement::GetAvailableSpace() const
{
    return pixelSize;
}

UIElement::~UIElement()
{
    if (parent)
    {
        auto &children = parent->children;
        children.erase(std::find(children.begin(), children.end(), GetPointer()));
        parent->OnRemoveChild(GetPointer());
    }
    auto children = UIElement::children;
    for (auto &child : children)
    {
        child.Delete();
    }
}

UISize UIElement::GetSize() const
{
    return size;
}

Vector2Int UIElement::GetPixelSize() const
{
    return pixelSize;
}

UIMargin UIElement::GetMargin() const
{
    return margin;
}

UIMarginPX UIElement::GetPixelMargin() const
{
    return pixelMargin;
}

void UIElement::SetMargin(UIMargin margin)
{
    UIElement::margin = margin;
    UpdateSize(true, false);
}

const Pointer<UIElement> &UIElement::GetParent() const
{
    return parent;
}

const std::vector<Pointer<UIElement>> UIElement::GetChildren() const
{
    return children;
}

bool UIElement::GetState() const
{
    return isOnScene;
}
