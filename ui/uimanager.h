#ifndef _VENGINE_UI_UIMANAGER_
#define _VENGINE_UI_UIMANAGER_

#include "../pointer.h"
#include "uielement.h"
#include <type_traits>
#include "../rendering/renderer.h"
#include "../structs/vector3.h"
#include "../structs/vector2.h"

template <class T>
concept ValidUIElement = std::is_convertible_v<T*, UIElement*> && requires()
{
    new T();
};

class UIManager
{
private:
    struct Vertex
    {
        Vector3 position;
        const Vector2 uv;
    };

    static inline Vertex vertices[4] = {
        {{}, {0, 0}},
        {{}, {0, 1}},
        {{}, {1, 0}},
        {{}, {1, 1}}};
    static inline const GLuint indecies[4] = {0, 1, 2, 3};
    static inline GLuint vbo;
    static inline GLuint ibo;
    static inline GLuint vao;

    static bool Start();
    static void Stop();
    static void Draw();

public:
    /// @brief Creates a new UI element.
    /// @tparam T type of the element.
    /// @param parent parent of the element. Can be null.
    /// @param args arguments to pass to the element's constructor
    /// @returns Pointer to the new element
    template <ValidUIElement T>
    static Pointer<T> CreateElement(const Pointer<UIElement> &parent);
    /// Fills the VBO with positions of a quad of `size` at `position`.
    static void CreateQuad(Vector2Int position, Vector2Int size);

    friend class Renderer;
};

#include "uimanager.hxx"

#endif