#include "form.h"
#include "../../inputmanager.h"
#include "../../gameloop.h"
#include <algorithm>

void UIForm::Press(bool up)
{
    time = 0;
    direction = up ? 1 : -1;
}

void UIForm::Hold(bool up)
{
    time -= GameLoop::GetDeltaTime();
    if (time <= 0)
    {
        if (up && direction > 0)
        {
            MoveUp();
        }
        else if (direction < 0)
        {
            MoveDown();
        }
        time = timeout;
        childInputs[selected]->Focus();
    }
}

void UIForm::MoveDown()
{
    selected++;
    selected = selected >= childInputs.size() ? childInputs.size() - 1 : selected;
}

void UIForm::MoveUp()
{
    if (selected == 0)
    {
        return;
    }
    selected--;
}

void UIForm::OnAddChild(const Pointer<UIElement> &child)
{
    UIContainer::OnAddChild(child);
    auto input = child.DynamicCast<UIInput>();
    if (input)
    {
        childInputs.push_back(input);
        if (childInputs.size() == 1)
        {
            input->Focus();
        }
    }
}

void UIForm::OnRemoveChild(const Pointer<UIElement> &child)
{
    UIContainer::OnRemoveChild(child);
    auto input = child.DynamicCast<UIInput>();
    if (input)
    {
        auto it = std::find(childInputs.begin(), childInputs.end(), child.DynamicCast<UIInput>());
        if (it != childInputs.end())
        {
            if (it <= childInputs.begin() + selected)
            {
                selected--;
            }
            childInputs.erase(it);
        }
    }
}

void UIForm::OnEnable()
{
    CreateCallbacks();
    if (selected < childInputs.size())
    {
        childInputs[selected]->Focus();
    }
}

void UIForm::OnDisable()
{
    ClearCallbacks();
}

void UIForm::CreateCallbacks()
{
    if (hasCallbacks)
    {
        return;
    }
    clearCallbacks[0] = InputManager::RegisterPressCallback(SDL_SCANCODE_UP, [this](){ this->Press(true); });
    clearCallbacks[1] = InputManager::RegisterPressCallback(SDL_SCANCODE_DOWN, [this](){ this->Press(false); });
    clearCallbacks[2] = InputManager::RegisterHoldCallback(SDL_SCANCODE_UP, [this](){ this->Hold(true); });
    clearCallbacks[3] = InputManager::RegisterHoldCallback(SDL_SCANCODE_DOWN, [this](){ this->Hold(false); });
    hasCallbacks = true;
}

void UIForm::ClearCallbacks()
{
    if (!hasCallbacks)
    {
        return;
    }
    for (const auto &clear : clearCallbacks)
    {
        clear();
    }
    hasCallbacks = false;
}

UIForm::UIForm(float timeout) : timeout(timeout) {}

UIForm::~UIForm()
{
    ClearCallbacks();
}