#ifndef _VENGINE_UI_ELEMENTS_FORM_
#define _VENGINE_UI_ELEMENTS_FORM_

#include "container.h"
#include "../uiinput.h"
#include <functional>
#include <array>

/// @brief UI element containing a group of inputs, that can be select with up and down arrows.
class UIForm : public UIContainer
{
private:
    std::vector<Pointer<UIInput>> childInputs;
    size_t selected = 0;
    std::array<std::function<void()>, 4> clearCallbacks;
    bool hasCallbacks = false;
    float time = 0;
    float timeout;
    int direction = 0;

    void Press(bool up);
    void Hold(bool up);
    void MoveDown();
    void MoveUp();

protected:
    void OnAddChild(const Pointer<UIElement> &child) override;
    void OnRemoveChild(const Pointer<UIElement> &child) override;
    void OnEnable() override;
    void OnDisable() override;
    void CreateCallbacks();
    void ClearCallbacks();

public:
    /// @brief Constructs a new UIForm.
    /// @param timeout time that has to pass to change selection when continuosly holding up or down arrows.
    UIForm(float timeout = 0.3f);
    ~UIForm();
};

#endif