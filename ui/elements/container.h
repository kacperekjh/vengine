#ifndef _VENGINE_UI_ELEMENTS_CONTAINER_
#define _VENGINE_UI_ELEMENTS_CONTAINER_

#include <vector>
#include "../uielement.h"
#include "../uialignment.h"

/// @brief Enum representing direction of a layout.
enum class UIDirection
{
    LeftToRight,
    TopToBottom
};

/// @brief UI element that lays out multiple child elements.
class UIContainer : public UIElement
{
private:
    UIDirection direction = UIDirection::LeftToRight;
    UIAlignment centerH = UIAlignment::Begin;
    UIAlignment centerV = UIAlignment::Begin;
    bool isVertical = false;
    UIMargin padding = {0_px, 0_px, 0_px, 0_px};
    UIMarginPX pixelPadding = {0, 0, 0, 0};
    int verticalPadding = 0;
    int horizontalPadding = 0;

    Vector2Int ConvertPosition(int position, Vector2Int size) const;

protected:
    void OnSizeChange() override;
    void OnAddChild(const Pointer<UIElement> &child) override;
    void OnRemoveChild(const Pointer<UIElement> &child) override;
    void Draw(Vector2Int position) override;
    Vector2Int GetPreferredSize() const override;
    Vector2Int GetAvailableSpace() const override;

public:
    /// @brief Changes direction of the layout.
    void SetDirection(UIDirection direction);
    /// @brief Changes alignment of the layout.
    void SetAlignment(UIAlignment horizontal, UIAlignment vertical);
    /// @brief Changes the padding of this container.
    void SetPadding(UIMargin padding);
    using UIElement::SetSize;
};

#endif