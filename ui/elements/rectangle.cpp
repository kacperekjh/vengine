#include "rectangle.h"
#include "../../rendering/renderer.h"
#include "../uimanager.h"

void UIRectangle::OnSizeChange()
{
    if (material)
    {
        material->SetUniform<Uniform2i>("elementSize", GetPixelSize().x, GetPixelSize().y);
    }
}

void UIRectangle::Draw(Vector2Int position)
{
    if (!material || !material->IsValid())
    {
        return;
    }
    glUseProgram(material->GetProgramID());
    UIManager::CreateQuad(position, GetPixelSize());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 20, (void *)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 20, (void *)sizeof(Vector3));
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glUseProgram(0);
}

void UIRectangle::SetMaterial(const Pointer<Material> &material)
{
    UIRectangle::material = material;
    if (material)
    {
        material->SetUniform<Uniform2i>("elementSize", GetPixelSize().x, GetPixelSize().y);
    }
}