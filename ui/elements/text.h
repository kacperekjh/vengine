#ifndef _VENGINE_UI_ELEMENTS_TEXT_
#define _VENGINE_UI_ELEMENTS_TEXT_

#include <SDL2/SDL_ttf.h>
#include <string>
#include "../uielement.h"
#include "../uialignment.h"
#include "../../structs/color.h"
#include "../fontmanager.h"
#include "../../rendering/texture2d.h"

class UIText : public virtual UIElement
{
private:
    static inline Pointer<Material> materialBase = nullptr;
    Pointer<Material> material = nullptr;
    std::string text = {};
    Color color = {255, 255, 255};
    Pointer<Font> font = FontManager::GetDefaultFont();
    int ptSize = 21;
    UIAlignment alignH = UIAlignment::Begin;
    UIAlignment alignV = UIAlignment::Begin;
    mutable SDL_Surface *textSurface = nullptr;
    mutable bool textureUpdated = true;
    Pointer<Texture2D> texture = nullptr;

    void FreeSurface() const;
    Vector2Int Align(Vector2Int size) const;
    void RenderText() const;

protected:
    void Draw(Vector2Int position) override;
    Vector2Int GetPreferredSize() const override;

public:
    UIText();
    ~UIText();
    /// @brief Changes contents of the text.
    void SetText(std::string_view text);
    /// @brief Changes font of the text.
    void SetFont(Pointer<Font> font);
    /// @brief Changes the size of the font.
    void SetFontSize(int ptSize);
    /// @brief Changes color of the text.
    void SetTextColor(Color color);
    /// @brief Changes alignment of the text.
    void SetTextAlignment(UIAlignment horizontal, UIAlignment vertical);
    using UIElement::SetSize;
};

#endif