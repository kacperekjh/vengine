#include <cmath>
#include "container.h"
#include "../../rendering/renderer.h"

Vector2Int UIContainer::ConvertPosition(int position, Vector2Int size) const
{
    switch (direction)
    {
    case UIDirection::LeftToRight:
        return {position, UIAlign(centerV, size.y, UIContainer::GetPixelSize().y - verticalPadding) + pixelPadding.top};
    case UIDirection::TopToBottom:
        return {UIAlign(centerH, size.x, UIContainer::GetPixelSize().x - horizontalPadding) + pixelPadding.left, position};
    }
    return {};
}

void UIContainer::Draw(Vector2Int containerPosition)
{
    int totalSize = 0;
    for (const auto &child : GetChildren())
    {
        UIMarginPX margin = child->GetPixelMargin();
        totalSize += isVertical ? child->GetPixelSize().y + margin.top + margin.bottom : child->GetPixelSize().x + margin.left + margin.right;
    }
    Vector2Int offset = isVertical 
        ? Vector2Int(0, UIAlign(centerV, totalSize, UIContainer::GetPixelSize().y - verticalPadding) + pixelPadding.top)
        : Vector2Int(UIAlign(centerH, totalSize, UIContainer::GetPixelSize().x - horizontalPadding) + pixelPadding.left, 0);
    int position = 0;
    for (const auto &child : GetChildren())
    {
        Vector2Int size = child->GetPixelSize();
        UIMarginPX margin = child->GetPixelMargin();
        Vector2Int totalSize = size + Vector2Int(margin.left + margin.right, margin.top + margin.bottom);
        Vector2Int pos = ConvertPosition(position, totalSize);
        pos += offset + containerPosition + Vector2Int(margin.left, margin.top);
        child->Draw(pos);
        position += isVertical ? size.y + margin.top + margin.bottom : size.x + margin.left + margin.right;
    }
}

void UIContainer::OnAddChild(const Pointer<UIElement> &child)
{
    UpdateSize();
}

void UIContainer::OnRemoveChild(const Pointer<UIElement> &child)
{
    UpdateSize();
}

void UIContainer::OnSizeChange()
{
    pixelPadding = padding.Evaluate(GetPixelSize(), {});
}

Vector2Int UIContainer::GetPreferredSize() const
{
    Vector2Int preferred = {};
    for (const auto &child : GetChildren())
    {
        auto margin = child->GetPixelMargin();
        auto size = child->GetPixelSize() + Vector2Int(margin.left + margin.right, margin.top + margin.bottom);
        if (isVertical)
        {
            preferred.y += size.y;
            if (size.x > preferred.x)
            {
                preferred.x = size.x;
            }
        }
        else
        {
            preferred.x += size.x;
            if (size.y > preferred.y)
            {
                preferred.y = size.y;
            }
        }
    }
    preferred += Vector2Int(pixelPadding.left + pixelPadding.right, pixelPadding.top + pixelPadding.bottom);
    return preferred;
}

Vector2Int UIContainer::GetAvailableSpace() const
{
    return {GetPixelSize().x - horizontalPadding, GetPixelSize().y - verticalPadding};
}

void UIContainer::SetDirection(UIDirection direction)
{
    UIContainer::direction = direction;
    isVertical = direction == UIDirection::TopToBottom;
    UpdateSize();
}

void UIContainer::SetAlignment(UIAlignment horizontal, UIAlignment vertical)
{
    centerH = horizontal;
    centerV = vertical;
    UpdateSize();
}

void UIContainer::SetPadding(UIMargin padding)
{
    UIContainer::padding = padding;
    pixelPadding = padding.Evaluate(GetPixelSize(), {});
    verticalPadding = pixelPadding.top + pixelPadding.bottom;
    horizontalPadding = pixelPadding.left + pixelPadding.right;
    UpdateSize();
}