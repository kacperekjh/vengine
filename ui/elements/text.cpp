#include "text.h"
#include "../fontmanager.h"
#include "../uimanager.h"
#include "../../rendering/renderer.h"
#include "../../resources/resourcemanager.h"

void UIText::FreeSurface() const
{
    if (textSurface)
    {
        SDL_FreeSurface(textSurface);
    }
    textSurface = nullptr;
}

Vector2Int UIText::Align(Vector2Int size) const
{
    Vector2Int totalSize = GetPixelSize();
    return {UIAlign(alignH, size.x, totalSize.x), UIAlign(alignV, size.y, totalSize.y)};
}

void UIText::RenderText() const
{
    if (!textSurface)
    {
        textureUpdated = true;
        FreeSurface();
        if (font && font->IsValid())
        {
            textSurface = TTF_RenderUTF8_Blended_Wrapped(font->GetFont(ptSize), text.c_str(), {color.r, color.g, color.b, color.a}, 0);
        }
        else
        {
            textSurface = nullptr;
        }
    }
}

void UIText::Draw(Vector2Int position)
{
    if (!font || !font->IsValid() || text.size() == 0 || !material->IsValid())
    {
        return;
    }
    RenderText();
    if (textureUpdated)
    {
        texture->SetData(textSurface);
        textureUpdated = false;
    }
    glUseProgram(material->GetProgramID());
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 20, (void *)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 20, (void *)sizeof(Vector3));
    Vector2Int offset = Align({textSurface->w, textSurface->h});
    UIManager::CreateQuad(position + offset, {textSurface->w, textSurface->h});
    texture->Bind(GL_TEXTURE0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    texture->Unbind();
}

Vector2Int UIText::GetPreferredSize() const
{
    if (!font || text.size() == 0)
    {
        return {};
    }
    RenderText();
    if (textSurface)
    {
        return {textSurface->w, textSurface->h};
    }
    else
    {
        return {0, 0};
    }
}

UIText::UIText()
{
    if (!materialBase)
    {
        materialBase = ResourceManager::CreateResource<Material>();
        materialBase->SetShader(ResourceManager::OpenResourceFromFile<VertexShader>("shaders/ui/ui.vert"));
        materialBase->SetShader(ResourceManager::OpenResourceFromFile<FragmentShader>("shaders/ui/text.frag"));
        if (!materialBase->Link())
        {
            SDL_LogError(0, "Failed to create UIText base material.");
        }
        materialBase->SetUniform<Uniform1i>("inTexture", 0);
    }
    material = materialBase->Clone();
    texture = ResourceManager::CreateResource<Texture2D>();
}

UIText::~UIText()
{
    texture.Delete();
    FreeSurface();
}

void UIText::SetText(std::string_view text)
{
    UIText::text = text;
    UpdateSize();
    FreeSurface();
}

void UIText::SetFont(Pointer<Font> font)
{
    UIText::font = font;
    if (!font && !font->IsValid())
    {
        SDL_LogWarn(0, "Missing or invalid font.");
    }
    UpdateSize();
    FreeSurface();
}

void UIText::SetFontSize(int ptSize)
{
    UIText::ptSize = ptSize;
    UpdateSize();
    FreeSurface();
}

void UIText::SetTextColor(Color color)
{
    UIText::color = color;
    FreeSurface();
}

void UIText::SetTextAlignment(UIAlignment horizontal, UIAlignment vertical)
{
    alignH = horizontal;
    alignV = vertical;
    FreeSurface();
}
