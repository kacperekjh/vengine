#ifndef _VENGINE_UI_ELEMENTS_RECTANGE_
#define _VENGINE_UI_ELEMENTS_RECTANGE_

#include "../uielement.h"
#include "../../structs/color.h"

/// @brief UI element representing a simple rectangle.
class UIRectangle : public virtual UIElement
{
private:
    Pointer<Material> material = nullptr;

protected:
    void OnSizeChange() override;
    void Draw(Vector2Int position) override;

public:
    /// @brief Changes the material of this rectangle.
    virtual void SetMaterial(const Pointer<Material> &material);
    using UIElement::SetSize;
};

#endif
