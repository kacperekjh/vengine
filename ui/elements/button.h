#ifndef _VENGINE_UI_ELEMENTS_BUTTON_
#define _VENGINE_UI_ELEMENTS_BUTTON_

#include "../uiinput.h"
#include "rectangle.h"
#include "text.h"
#include <functional>

/// @brief UI input element that can be triggered when it's selected and space key is pressed.
class UIButton : public UIInput, public UIRectangle, public UIText
{
private:
    std::function<void()> onClick;
    std::function<void()> clearCallback;
    bool hasCallback = false;
    Pointer<Material> material = {};
    Pointer<Material> selectedMaterial = {};

protected:
    void Draw(Vector2Int position) override;
    Vector2Int GetPreferredSize() const override;
    void OnDisable() override;
    void CreateCallback();
    void ClearCallback();

public:
    /// @brief Constructs a new UIButton.
    UIButton();
    ~UIButton();
    void OnFocus() override;
    void OnUnfocus() override;
    /// @brief Changes color of this button when it's unfocused.
    void SetMaterial(const Pointer<Material> &material) override;
    /// @brief Changes color of this button when it's focused.
    void SetSelectedMaterial(const Pointer<Material> &material);
    /// @brief Changes the function that gets executed when the button is clicked.
    void SetCallback(std::function<void()> onClick);
    using UIElement::SetSize;
};

#endif