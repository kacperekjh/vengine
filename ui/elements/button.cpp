#include "button.h"
#include "../../inputmanager.h"

void UIButton::Draw(Vector2Int position)
{
    UIRectangle::Draw(position);
    UIText::Draw(position);
}

Vector2Int UIButton::GetPreferredSize() const
{
    return UIText::GetPreferredSize() + Vector2Int(2, 2);
}

void UIButton::OnDisable()
{
    UIInput::OnDisable();
    ClearCallback();
}

void UIButton::CreateCallback()
{
    if (!hasCallback)
    {
        clearCallback = InputManager::RegisterReleaseCallback(SDL_SCANCODE_SPACE, onClick);
        hasCallback = true;
    }
}

void UIButton::ClearCallback()
{
    if (hasCallback)
    {
        clearCallback();
        hasCallback = false;
    }
}

UIButton::UIButton()
{
    SetTextAlignment(UIAlignment::Center, UIAlignment::Center);
}

UIButton::~UIButton()
{
    ClearCallback();
}

void UIButton::OnFocus()
{
    CreateCallback();
    UIRectangle::SetMaterial(selectedMaterial);
}

void UIButton::OnUnfocus()
{
    ClearCallback();
    UIRectangle::SetMaterial(material);
}

void UIButton::SetMaterial(const Pointer<Material> &material)
{
    UIButton::material = material;
    if (!IsFocused())
    {
        UIRectangle::SetMaterial(material);
    }
}

void UIButton::SetSelectedMaterial(const Pointer<Material> &material)
{
    selectedMaterial = material;
    if (IsFocused())
    {
        UIRectangle::SetMaterial(material);
    }
}

void UIButton::SetCallback(std::function<void()> onClick)
{
    UIButton::onClick = onClick;
}