#ifndef _VENGINE_UI_UISIZEEXPRESSION_
#define _VENGINE_UI_UISIZEEXPRESSION_

#include <vector>
#include "uidimension.h"

/// @brief Struct representing a collection of UIDimensions used for adding and subtracting sizes with different units.
struct UISizeExpression
{
    std::vector<UIDimension> elements;

    UISizeExpression();
    UISizeExpression(const UIDimension &dimension);
    UISizeExpression(const std::vector<UIDimension> &elements);
    /// @brief Evaluates the size in pixels inside a container with dimensions `containerSize`.
    int Evaluate(int containerSize, int preferred) const;
    UISizeExpression operator+(const UIDimension &b) const;
    UISizeExpression operator-(const UIDimension &b) const;
};

UISizeExpression operator+(const UIDimension &a, const UIDimension &b);
UISizeExpression operator-(const UIDimension &a, const UIDimension &b);

#endif