#include "fontmanager.h"
#include "notosans-medium.h"
#include "../resources/resourcemanager.h"

bool FontManager::Start()
{
    if (TTF_Init())
    {
        SDL_LogError(SDL_LOG_CATEGORY_ERROR, TTF_GetError());
        return false;
    }
    defaultFont = ResourceManager::OpenResourceFromMem<Font>(NotoSans_Medium_ttf, NotoSans_Medium_ttf_size);
    return true;
}

void FontManager::Stop()
{
    TTF_Quit();
}

const Pointer<Font> &FontManager::GetDefaultFont()
{
    return defaultFont;
}