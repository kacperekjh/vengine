#include "uimargin.h"

bool UIMarginPX::operator==(const UIMarginPX &b) const
{
    return left == b.left && top == b.top && right == b.right && bottom == b.bottom;
}

bool UIMarginPX::operator!=(const UIMarginPX &b) const
{
    return left != b.left || top != b.top || right != b.right || bottom != b.bottom;
}

UIMargin::UIMargin(const UISizeExpression &margin) : left(margin), top(margin), right(margin), bottom(margin) {}

UIMargin::UIMargin(const UISizeExpression &left, const UISizeExpression &top, const UISizeExpression &right, const UISizeExpression &bottom) : left(left), top(top), right(right), bottom(bottom) {}

UIMarginPX UIMargin::Evaluate(Vector2Int containerSize, Vector2Int preferredSize) const
{
    return 
    {
        left.Evaluate(containerSize.x, preferredSize.x),
        top.Evaluate(containerSize.y, preferredSize.y),
        right.Evaluate(containerSize.x, preferredSize.x),
        bottom.Evaluate(containerSize.y, preferredSize.y)
    };
}