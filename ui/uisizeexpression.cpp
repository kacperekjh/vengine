#include "uisizeexpression.h"

UISizeExpression::UISizeExpression() {}

UISizeExpression::UISizeExpression(const UIDimension &dimension) : elements{{dimension}} {}

UISizeExpression::UISizeExpression(const std::vector<UIDimension> &elements) : elements(elements) {}

int UISizeExpression::Evaluate(int containerSize, int preferred) const
{
    int size = 0;
    for (const auto &element : elements)
    {
        size += element.Evaluate(containerSize, preferred);
    }
    return size;
}

UISizeExpression UISizeExpression::operator+(const UIDimension &b) const
{
    UISizeExpression expr = UISizeExpression();
    expr.elements = elements;
    expr.elements.push_back(b);
    return expr;
}

UISizeExpression UISizeExpression::operator-(const UIDimension &b) const
{
    UISizeExpression expr = UISizeExpression();
    expr.elements = elements;
    UIDimension element = b;
    element.size *= -1;
    expr.elements.push_back(element);
    return expr;
}

UISizeExpression operator+(const UIDimension &a, const UIDimension &b)
{
    return {{a, b}};
}

UISizeExpression operator-(const UIDimension &a, const UIDimension &b)
{
    UIDimension element = b;
    element.size *= -1;
    return {{a, element}};
}
