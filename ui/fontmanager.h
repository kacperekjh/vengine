#ifndef _VENGINE_UI_FONTMANAGER_
#define _VENGINE_UI_FONTMANAGER_

#include <map>
#include <string>
#include <SDL2/SDL_ttf.h>
#include "font.h"
#include "../pointer.h"

class FontManager
{
private:
    static inline Pointer<Font> defaultFont = nullptr;

    static bool Start();
    static void Stop();

public:
    /// @brief Returns the default font.
    static const Pointer<Font> &GetDefaultFont();

    friend class UIManager;
};

#endif