#ifndef _VENGINE_UI_UIMARGIN_
#define _VENGINE_UI_UIMARGIN_

#include "uisizeexpression.h"
#include "../structs/vector2int.h"

struct UIMarginPX
{
    int left;
    int top;
    int right;
    int bottom;

    bool operator==(const UIMarginPX &b) const;
    bool operator!=(const UIMarginPX &b) const;
};

struct UIMargin
{
    UISizeExpression left;
    UISizeExpression top;
    UISizeExpression right;
    UISizeExpression bottom;

    UIMargin(const UISizeExpression &margin);
    UIMargin(const UISizeExpression &left = 0_px, const UISizeExpression &top = 0_px, const UISizeExpression &right = 0_px, const UISizeExpression &bottom = 0_px);
    UIMarginPX Evaluate(Vector2Int containerSize, Vector2Int preferredSize) const;
};

#endif