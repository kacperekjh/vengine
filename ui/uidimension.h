#ifndef _VEGINE_UI_UIDIMENSION_
#define _VEGINE_UI_UIDIMENSION_

enum class UIUnit
{
    Pixel,
    Percent,
    Auto
};

struct UIDimension
{
    float size;
    UIUnit unit;

    /// @brief Evaluates the size in pixel inside a container with dimension `size`.
    int Evaluate(int containerSize, int preferred) const;
};

/// @brief Returns UIDimension equal `size`px
constexpr UIDimension operator""_px(unsigned long long size)
{
    return {(float)size, UIUnit::Pixel};
}

/// @brief Returns UIDimension equal `size`%.
constexpr UIDimension operator""_pct(long double size)
{
    return {(float)(size / 100), UIUnit::Percent};
}

/// @brief Returns UIDimension equal `size`%.
constexpr UIDimension operator""_pct(unsigned long long size)
{
    return {(float)size / 100, UIUnit::Percent};
}

constexpr UIDimension size_auto = {0, UIUnit::Auto};

#endif