#include "inputmanager.h"
#include <stdexcept>
#include <cstring>

bool InputManager::Start()
{
    SDL_ShowCursor(false);
    pressed = SDL_GetKeyboardState(&size);
    oldPressed = new Uint8[size];
    memset(oldPressed, 0, size);
    return true;
}

void InputManager::Stop()
{
    if (oldPressed)
    {
        delete[] oldPressed;
        oldPressed = nullptr;
    }
    pressCallbacks.clear();
    holdCallbacks.clear();
    releaseCallbacks.clear();
}

void InputManager::Update()
{
    for (auto &callback : pressCallbacks)
    {
        if (IsPressed(callback.first))
        {
            callback.second();
        }
    }
    for (auto &callback : holdCallbacks)
    {
        if (IsHeld(callback.first))
        {
            callback.second();
        }
    }
    for (auto &callback : releaseCallbacks)
    {
        if (IsReleased(callback.first))
        {
            callback.second();
        }
    }
    for (int i = 0; i < size; i++)
    {
        oldPressed[i] = pressed[i];
    }
}

bool InputManager::IsPressed(SDL_Scancode key)
{
    if (key < 0 || key >= size)
    {
        throw std::invalid_argument("Invalid scancode!");
    }
    return !oldPressed[key] && pressed[key];
}

bool InputManager::IsHeld(SDL_Scancode key)
{
    if (key < 0 || key >= size)
    {
        throw std::invalid_argument("Invalid scancode!");
    }
    return oldPressed[key] && pressed[key];
}

bool InputManager::IsReleased(SDL_Scancode key)
{
    if (key < 0 || key >= size)
    {
        throw std::invalid_argument("Invalid scancode!");
    }
    return oldPressed[key] && !pressed[key];
}

std::function<void()> InputManager::RegisterPressCallback(SDL_Scancode key, const std::function<void()> &callback)
{
    pressCallbacks.push_front({key, callback});
    auto it = pressCallbacks.begin();
    return [it]()
    {
        InputManager::pressCallbacks.erase(it);
    };
}

std::function<void()> InputManager::RegisterHoldCallback(SDL_Scancode key, const std::function<void()> &callback)
{
    holdCallbacks.push_front({key, callback});
    auto it = holdCallbacks.begin();
    return [it]()
    {
        InputManager::holdCallbacks.erase(it);
    };
}

std::function<void()> InputManager::RegisterReleaseCallback(SDL_Scancode key, const std::function<void()> &callback)
{
    releaseCallbacks.push_front({key, callback});
    auto it = releaseCallbacks.begin();
    return [it]()
    {
        InputManager::releaseCallbacks.erase(it);
    };
}