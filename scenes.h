#ifndef _VENGINE_GAMEOBJECT_SCENES_
#define _VENGINE_GAMEOBJECT_SCENES_

/// @brief Class initializing all scenes and their gameobject. You have to implement it yourself.
class Scenes
{
private:
    static void CreateScenes();

    friend class ObjectManager;
};

#endif