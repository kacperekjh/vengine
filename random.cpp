#include "random.h"

bool Random::GetRandomBool()
{
    return GetRandomNumber<unsigned>(0, 1) == 0;
}