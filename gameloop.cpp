#include "gameloop.h"
#include "rendering/renderer.h"
#include "gameobjects/objectmanager.h"
#include "inputmanager.h"
#include "audio/audiomanager.h"
#include "resources/resourcemanager.h"
#include <SDL2/SDL.h>
#include <chrono>

bool (*const GameLoop::subsystemStarts[])() = {
    ResourceManager::Start,
    InputManager::Start,
    Renderer::Start,
    AudioManager::Start,
    ObjectManager::Start
    };

void (*const GameLoop::subsystemUpdates[])() = {
    ResourceManager::Update,
    InputManager::Update,
    Renderer::Update,
    ObjectManager::Update
    };

void (*const GameLoop::subsystemStops[])() = {
    ObjectManager::Stop,
    AudioManager::Stop,
    InputManager::Stop,
    ResourceManager::Stop,
    Renderer::Stop
};

void GameLoop::StartLoop()
{
    if (isRunning)
    {
        return;
    }
    isRunning = true; 
    for (const auto start : subsystemStarts)
    {
        if (!start())
        {
            isRunning = false;
            SDL_LogCritical(SDL_LOG_CATEGORY_ERROR, "Failed to initialize subsystems.\n");
            break;
        }
    }
    while (isRunning)
    {
        auto start = std::chrono::high_resolution_clock::now();
        // Check if the close button was pressed
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.window.event == SDL_WINDOWEVENT_CLOSE)
            {
                EndLoop();
            }
        }
        for (const auto update : subsystemUpdates)
        {
            update();
        }
        auto end = std::chrono::high_resolution_clock::now();
        deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() / 1e6;
    }
    for (const auto stop : subsystemStops)
    {
        stop();
    }
}

void GameLoop::EndLoop()
{
    isRunning = false;
}

bool GameLoop::GetState()
{
    return isRunning;
}

float GameLoop::GetDeltaTime()
{
    return deltaTime;
}