#ifndef _VENGINE_RESOURCES_RWOPSRESOURCE_
#define _VENGINE_RESOURCES_RWOPSRESOURCE_

#include "resource.h"

/// @brief Base class for all resource types that use `SDL_RWops` as their source.
class RWopsResource : public Resource
{
private:
    void ChangeSource(SDL_RWops *src);

protected:
    SDL_RWops *src;

    /// @brief Constructs Resource. `src` is handled and disposed by this class.
    RWopsResource(SDL_RWops *src);

public:
    ~RWopsResource();
    virtual void OnSourceChange(SDL_RWops *src);

    friend class ResourceManager;
};

#endif