#include "rwopsresource.h"

void RWopsResource::ChangeSource(SDL_RWops *src)
{
    if (RWopsResource::src)
    {
        SDL_RWclose(RWopsResource::src);
    }
    RWopsResource::src = src;
    OnSourceChange(src);
}

RWopsResource::RWopsResource(SDL_RWops *src) : src(src) {}

RWopsResource::~RWopsResource()
{
    if (src)
    {
        SDL_RWclose(src);
    }
}

void RWopsResource::OnSourceChange(SDL_RWops *src) {}