#ifndef _VENGINE_RESOURCES_RESOURCE_MANAGER_
#define _VENGINE_RESOURCES_RESOURCE_MANAGER_

#include <string>
#include <SDL2/SDL.h>
#include "rwopsresource.h"
#include "../pointer.h"
#include <map>
#include <concepts>
#include <typeindex>
#include <vector>
#include <filesystem>

#ifdef ENABLE_RESOURCE_WATCH
#ifdef __linux__
#include <sys/inotify.h>
#include <unistd.h>
#include <fcntl.h>
#include <functional>
#else
#error Resource watch is currently only supported on Linux using inotify :3.
#endif
#endif

namespace ResourceConcepts
{
    template <class T>
    concept RWopsResourceType = std::is_convertible_v<T *, Resource *>;
    template <class T>
    concept NonRWopsResourceType = std::is_convertible_v<T *, Resource *> && !std::is_convertible_v<T *, RWopsResource *>;
}

/// @brief Class responsible for handling various resources through the game.
class ResourceManager
{
private:
    inline static std::map<std::type_index, std::map<std::string, Pointer<RWopsResource>>> resources;
    inline static std::vector<Pointer<Resource>> nonRWopsResources;
#ifdef ENABLE_RESOURCE_WATCH
    struct Watch
    {
        std::string location;
        std::function<void(SDL_RWops*)> callback;
    };

    inline static std::map<int, Watch> watches;
    inline static int inotifyInstance;

    static void RemoveWatches();
#endif

    static bool Start();
    static void Update();
    static void Stop();
    static std::map<std::string, Pointer<RWopsResource>> &GetType(std::type_index index);

public:
    static constexpr std::string resourcePath = "./data/";

    /// @brief Opens a new read-only `SDL_RWops` for a file in the resource folder.
    /// @param location Path and name of the file in the resource folder
    static SDL_RWops *OpenRawResourceFromFile(std::string_view location);
    /// @brief Returns a canonical path to a resource located inside of the resource folder.
    static std::filesystem::path GetResourcePath(std::string_view location);
    /// @brief Opens a new resource object from a file in the resource folder.
    ///        The resources are stored for later, so calling this function multiple times for the same file
    ///        of the same type only open it once.
    /// @tparam T Type of the resource
    /// @param location Path and name of the file in the resource folder
    template <ResourceConcepts::RWopsResourceType T>
    static Pointer<T> OpenResourceFromFile(const std::string &location);
    /// @brief Opens a new resource object from a memory location.
    ///        The resources are stored for later, so calling this function multiple times for the same address
    ///        of the same type only open it once.
    /// @tparam T Type of the resource
    /// @param mem Address of the resource
    /// @param size Size of the resource
    template <ResourceConcepts::RWopsResourceType T>
    static Pointer<T> OpenResourceFromMem(const void *mem, size_t size);
    /// @brief Creates a new resource that takes no input parameters.
    /// @tparam T Type of the resource. Must not be convertible to `RWopsResource`.
    template <ResourceConcepts::NonRWopsResourceType T>
    static Pointer<T> CreateResource();

    friend class GameLoop;
};

#include "resourcemanager.hxx"

#endif