template <ResourceConcepts::RWopsResourceType T>
Pointer<T> ResourceManager::OpenResourceFromFile(const std::string &location)
{
    static_assert(requires(SDL_RWops *src) { T(src); }, "Resource derieving from RWopsResource has no constructor taking SDL_RWops* as a parameter.");

    auto &resources = GetType(std::type_index(typeid(T)));
    auto it = resources.find(location);
    if (it != resources.end())
    {
        return it->second.DynamicCast<T>();
    }
    Pointer<T> ptr = new T(OpenRawResourceFromFile(location));
    resources[location] = ptr;
#ifdef ENABLE_RESOURCE_WATCH
    int wd = inotify_add_watch(inotifyInstance, GetResourcePath(location).c_str(), IN_MODIFY);
    watches[wd] = {std::string(location), [ptr](SDL_RWops *src){ ptr->OnSourceChange(src); }};
#endif
    return ptr;
}

template <ResourceConcepts::RWopsResourceType T>
Pointer<T> ResourceManager::OpenResourceFromMem(const void *mem, size_t size)
{
    static_assert(requires(SDL_RWops *src) { T(src); }, "Resource derieving from RWopsResource has no constructor taking SDL_RWops* as a parameter.");

    auto &resources = GetType(std::type_index(typeid(T)));
    std::string id = std::string(sizeof(mem) + 1, '\0');
    char *bytes = (char*)&mem;
    for (unsigned i = 0; i < sizeof(mem); i++)
    {
        id[i] = bytes[i];
    }
    auto it = resources.find(id);
    if (it != resources.end())
    {
        return it->second.DynamicCast<T>();
    }
    Pointer<T> ptr = new T(SDL_RWFromConstMem(mem, size));
    resources[id] = ptr;
    return ptr;
}

template <ResourceConcepts::NonRWopsResourceType T>
Pointer<T> ResourceManager::CreateResource()
{
    static_assert(requires() { T(); }, "Resource has no parameterless constructor.");

    Pointer<T> ptr = new T();
    nonRWopsResources.push_back(ptr);
    return ptr;
}