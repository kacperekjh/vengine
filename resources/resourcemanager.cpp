#include "resourcemanager.h"

#ifdef ENABLE_RESOURCE_WATCH
void ResourceManager::RemoveWatches()
{
    for (auto &[watch, _] : watches)
    {
        inotify_rm_watch(inotifyInstance, watch);
    }
    watches.clear();
}
#endif

bool ResourceManager::Start()
{
#ifdef ENABLE_RESOURCE_WATCH
    inotifyInstance = inotify_init();
    fcntl(inotifyInstance, F_SETFL, fcntl(inotifyInstance, F_GETFL) | O_NONBLOCK);
#endif
    return true;
}

#include <iostream>

void ResourceManager::Update()
{
#ifdef ENABLE_RESOURCE_WATCH
    if (watches.size() == 0)
    {
        return;
    }
    char buf[4096];
    inotify_event *event;
    while (true)
    {
        ssize_t len = read(inotifyInstance, buf, sizeof(buf));
        if (len == -1 && errno != EAGAIN)
        {
            SDL_LogError(0, "An unknown error has occured when watching for resource changes. Removing all watches.");
            RemoveWatches();
            break;
        }
        if (len <= 0)
        {
            break;
        }
        for (char *ptr = buf; ptr < buf + len; ptr += sizeof(inotify_event) + event->len)
        {
            event = (inotify_event*)ptr;
            Watch &watch = watches[event->wd];
            SDL_LogInfo(0, "Updated resource %s", watch.location.c_str());
            watch.callback(OpenRawResourceFromFile(watch.location));
        }
    }
#endif
}

void ResourceManager::Stop()
{
#ifdef ENABLE_RESOURCE_WATCH
    close(inotifyInstance);
#endif
    for (auto &map : resources)
    {
        for (auto &resource : map.second)
        {
            resource.second.Delete();
        }
    }
    resources.clear();
    for (auto &resource : nonRWopsResources)
    {
        resource.Delete();
    }
    nonRWopsResources.clear();
}

std::map<std::string, Pointer<RWopsResource>> &ResourceManager::GetType(std::type_index index)
{
    auto it1 = resources.find(index);
    if (it1 == resources.end())
    {
        it1 = resources.insert({index, {}}).first;
    }
    return it1->second;
}

SDL_RWops *ResourceManager::OpenRawResourceFromFile(std::string_view location)
{
    SDL_RWops *src = SDL_RWFromFile(GetResourcePath(location).c_str(), "r");
    if (!src)
    {
        SDL_LogError(0, SDL_GetError());
    }
    return src;
}

std::filesystem::path ResourceManager::GetResourcePath(std::string_view location)
{
    std::filesystem::path path = resourcePath;
    path /= location;
    path = std::filesystem::weakly_canonical(path);
    return path;
}