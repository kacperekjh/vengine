#ifndef _VENGINE_RESOURCES_RESOURCE_
#define _VENGINE_RESOURCES_RESOURCE_

#include <SDL2/SDL.h>
#include <string>

/// @brief Base class for all resource types.
class Resource
{
public:
    Resource();
    Resource(const Resource &b) = delete;
    Resource &operator=(const Resource &b) = delete;
    virtual ~Resource();
    /// @brief Returns if this resource is valid.
    virtual bool IsValid() const = 0;
};

#endif