#version 420 core

in vec2 uv;
out vec4 LFragment;

uniform sampler2D inTexture;

void main()
{
    LFragment = texture(inTexture, uv);
}