#version 420 core

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 uvPosition;

out vec2 uv;

uniform ivec2 resolution;

void main()
{
    gl_Position = vec4((inPosition.x - resolution.x / 2) / resolution.x, -(inPosition.y - resolution.y / 2) / resolution.y, 1, 1); 
    uv = uvPosition;
}