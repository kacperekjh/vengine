#version 420 core

in vec2 uv;
out vec4 LFragment;

uniform float borderWidth;
uniform ivec2 elementSize;
uniform ivec2 resolution;
uniform vec4 color;

void main()
{
    vec2 realPos = uv;
    if (realPos.x > 0.5)
    {
        realPos.x = 1 - realPos.x;
    }
    if (realPos.y > 0.5)
    {
        realPos.y = 1 - realPos.y;
    }
    realPos *= elementSize;

    vec4 transparent = vec4(color.xyz, 0);
    float dst = min(realPos.x - borderWidth, realPos.y - borderWidth);
    LFragment = mix(color, transparent, dst);
}