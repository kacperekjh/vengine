#version 420 core

layout (lines) in;
layout (triangle_strip, max_vertices = 8) out;

out vec2 uv;
uniform float geomThickness;
const float epsilon = 1e-6;
uniform ivec2 resolution;
uniform float geomEndLength;

vec4 CalculateDirection(vec4 a, vec4 b)
{
    vec4 dir = b - a;
    return normalize(dir);
}

void Emit(vec4 point, vec4 dir, vec4 perpDir, vec2 uvPos)
{
    const float aspectRatio = float(resolution.x) / float(resolution.y);
    vec4 position = point;
    dir /= mix(1, aspectRatio, clamp(abs(normalize(dir.xy).x), 0, 1));
    vec4 offset = dir + perpDir;
    position += offset;
    gl_Position = position;
    uv = uvPos;
    EmitVertex();
}

void Emit(vec4 point, vec4 perpDir, vec2 uvPos)
{
    Emit(point, vec4(0, 0, 0, 0), perpDir, uvPos);
}

void main()
{
    vec4 a = gl_in[0].gl_Position;
    a /= a.w;
    vec4 b = gl_in[1].gl_Position;
    b /= b.w;
    vec4 dir = CalculateDirection(a, b);
    float len = length(dir.xy);
    if (len <= epsilon)
    {
        dir = vec4(1, 0, dir.z, 0);
    }
    else
    {
        dir.xyz /= len;
    }
    vec4 perpDir = vec4(dir.y, -dir.x, dir.z, dir.w) * geomThickness;
    dir *= geomThickness * geomEndLength;
    Emit(a, -dir, perpDir, vec2(0, 0));
    Emit(a, -dir, -perpDir, vec2(0, 1));
    Emit(a, perpDir, vec2(0.5, 0));
    Emit(a, -perpDir, vec2(0.5, 1));
    Emit(b, perpDir, vec2(0.5, 0));
    Emit(b, -perpDir, vec2(0.5, 1));
    Emit(b, dir, perpDir, vec2(0, 0));
    Emit(b, dir, -perpDir, vec2(0, 1));
    EndPrimitive();
}