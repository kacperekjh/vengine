#version 420 core

in vec2 uv;
out vec4 LFragment;

uniform vec4 fragColor;
uniform float fragExponent;
uniform float fragOffset;
uniform float nearClip;
uniform float farClip;
uniform bool enableFading;

// https://learnopengl.com/Advanced-OpenGL/Depth-testing
float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0;
    return (2.0 * nearClip * farClip) / (farClip + nearClip - z * (farClip - nearClip));
}

void main()
{
    float alpha = abs(uv.y - 0.5) * 2;
    if (uv.x < 0.5)
    {
        alpha = sqrt(pow(uv.x * 2 - 1, 2) + pow(alpha, 2));
    }
    alpha = 1 - alpha;
    float x = pow(fragOffset, fragExponent);
    alpha = clamp(pow(alpha, fragExponent) / x, 0, 1);
    if (enableFading)
    {
        alpha *= 1.0 - LinearizeDepth(gl_FragCoord.z) / farClip;
    }
    LFragment = vec4(fragColor.x, fragColor.y, fragColor.z, fragColor.w * alpha);
}