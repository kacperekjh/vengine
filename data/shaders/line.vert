#version 420 core

in vec3 inPosition;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

void main()
{
    mat4 mvp = projectionMatrix * modelViewMatrix;
    gl_Position = mvp * vec4(inPosition, 1); 
}