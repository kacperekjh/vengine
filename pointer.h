#ifndef _VENGINE_POINTER_
#define _VENGINE_POINTER_

#include <cstddef>
#include <concepts>

template <class T1, class T2>
concept PointerCastable = requires(T1 *a, T2 *b)
{
    a = b;
};

/// @brief Struct holding shared data between Pointer<T>s. Not meant to be used for anything else. 
struct PointerData
{
    unsigned references;
    bool deleted;
    void (*deleter)(void *ptr);
};

/// @brief A smart pointer class that automatically frees memory using reference counting.
///        Allows to delete the contained objects prematurely and to check their state.
///        The reference counting is NOT thread safe should not be used on multiple threads at once.
/// @tparam T Type of the object to store
template <class T>
class Pointer
{
private:
    T *ptr;
    PointerData *data;

    Pointer(PointerData *data, T *ptr);

    void Increment();
    void Decrement();
    static void SingleDelete(void *ptr);
    static void ArrayDelete(void *ptr);

public:
    /// @brief Null pointer constant. Allows to pass null pointers by reference.
    static const inline Pointer null = nullptr;

    /// @brief Construct the pointer from any pointer that can be implicitly converted into the required type.
    /// @tparam T2 Type of the pointer to contain.
    /// @param ptr Pointer to contain
    /// @param isArray Marks whether the pointer was created using `new[]` operator. Required for proper disposal of arrays.
    template <PointerCastable<T> T2>    
    Pointer(T2* ptr, bool isArray = false);
    /// @brief Construct the pointer from any pointer that can be implicitly converted into the required type. Allows for a custom delete function.
    /// @tparam T2 Type of the pointer to contain.
    /// @param ptr Pointer to contain
    /// @param deleter Function that will be called to dispose of the pointer.
    template <PointerCastable<T> T2>
    Pointer(T2 *ptr, void (*deleter)(void *ptr));
    Pointer(const Pointer<T> &b);
    /// @brief Constructs an empty pointer holding a null value.
    Pointer(std::nullptr_t);
    /// @brief Constructs an empty pointer holding a null value.
    Pointer();
    ~Pointer();

    T &operator*() const;
    T *operator->() const;
    T &operator[](size_t index) const;
    Pointer<T> &operator=(const Pointer<T> &b);
    /// @brief Returns true if the pointer contains a not-deleted, non-null value.
    operator bool() const;
    /// @brief Tries to implicity convert the pointer into type `T2`.
    /// @tparam T2 Type to convert to
    template <PointerCastable<T> T2>
    operator Pointer<T2>() const;
    explicit operator T*() const;
    /// @brief Checks where this pointer and pointer `b` contain the same pointer.
    template <class T2>
    bool operator==(const Pointer<T2> &b) const;
    /// @brief Checks if this pointer contains pointer equal to `b`.
    template <class T2>
    bool operator==(T2 *b) const;
    /// @brief Returns true if the pointer contains a not-deleted, non-null value.
    bool GetState() const;
    /// @brief Deletes the contained value.
    void Delete();
    /// @brief Converts the pointer into `T2` using static_cast().
    /// @tparam T2 Type to convert to
    template <class T2>
    Pointer<T2> StaticCast() const;
    /// @brief Converts the pointer into `T2` using dynamic_cast().
    /// @tparam T2 Type to convert to
    template <class T2>
    Pointer<T2> DynamicCast() const;
    /// @brief Converts the pointer into `T2` using reinterpret_cast().
    /// @tparam T2 Type to convert to
    template <class T2>
    Pointer<T2> ReinterpretCast() const;

    template <class T2>
    friend class Pointer;
};

#include "pointer.hxx"

#endif