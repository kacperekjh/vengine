#include "linetransform.h"

void LineTransform::UpdateLines()
{
    transformed.clear();
    transformed.reserve(source.size());
    for (auto &line : source)
    {
        transformed.push_back(line.Scale(scale, pivotPoint).Rotate(rotation, pivotPoint).Translate(position - pivotPoint));
    }
    transformID++;
}

void LineTransform::Update()
{
    auto &owner = GetOwner();
    if (position != owner->position || rotation != owner->rotation || scale != owner->scale || sourceChanged)
    {
        position = owner->position;
        rotation = owner->rotation;
        scale = owner->scale;
        sourceChanged = false;
        UpdateLines();
    }
}

void LineTransform::SetLines(const std::vector<Line3D> &lines)
{
    source = lines;
    sourceChanged = true;
}

void LineTransform::SetLines(const Line3D &line)
{
    source = {line};
    sourceChanged = true;
}

const std::vector<Line3D> &LineTransform::GetSource() const
{
    return source;
}

const std::vector<Line3D> &LineTransform::GetTransformed() const
{
    return transformed;
}

unsigned LineTransform::GetTransformID() const
{
    return transformID;
}

const Vector3 &LineTransform::GetPivotPoint() const
{
    return pivotPoint;
}

void LineTransform::SetPivotPoint(const Vector3 &point)
{
    pivotPoint = point;
    sourceChanged = true;
}