#ifndef _VENGINE_COMPONENTS_DATA_
#define _VENGINE_COMPONENTS_DATA_

#include "../gameobjects/component.h"
#include "../structs/line3d.h"

/// @brief Component that handles transformations of line data.
class LineTransform : public Component
{
private:
    unsigned transformID = 0;
    Vector3 position;
    Vector3 rotation;
    Vector3 scale;
    Vector3 pivotPoint = {0, 0, 0};
    std::vector<Line3D> source;
    bool sourceChanged = false;
    std::vector<Line3D> transformed;

    void UpdateLines();

protected:
    void Update() override;

public:
    /// @brief Changes the source line data.
    void SetLines(const std::vector<Line3D> &lines);
    /// @brief Changes the source line data.
    void SetLines(const Line3D &line);
    /// @brief Returns the source line data.
    const std::vector<Line3D> &GetSource() const;
    /// @brief Returns the transformed line data.
    const std::vector<Line3D> &GetTransformed() const;
    /// @brief Returns the transformation ID, that can be used to detect changes of the data.
    unsigned GetTransformID() const;
    /// @brief Returns the point around which the line is rotated and scaled.
    const Vector3 &GetPivotPoint() const;
    /// @brief Sets the point around which the line is rotated and scaled.
    void SetPivotPoint(const Vector3 &point);
};

#endif