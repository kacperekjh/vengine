template <class Func, class... Args>
void Material::SetUniform(std::string_view name, Args... args) requires(ValidUniform<Func, Args...>)
{
    if (linked)
    {
        fields[std::string(name)] = Field{Func::Set(GetUniformLocation(name, false), args...), Func::Set, false};
    }
    else
    {
        fields[std::string(name)] = Field{Func::MakeRaw(args...), Func::Set, false};
    }
}

template <class Func, class... Args>
void Material::TrySetUniform(std::string_view name, Args... args) requires(ValidUniform<Func, Args...>)
{
    if (linked)
    {
        fields[std::string(name)] = Field{Func::Set(GetUniformLocation(name, true), args...), Func::Set, true};
    }
    else
    {
        fields[std::string(name)] = Field{Func::MakeRaw(args...), Func::Set, true};
    }
}

template <class Func, class... Args>
void Material::SetGlobalUniform(std::string_view name, Args ...args) requires(ValidUniform<Func, Args...>)
{
    globalFields[std::string(name)] = Field{Func::MakeRaw(args...), Func::Set, true};
    for (const auto &material : materials)
    {
        if (material->linked)
        {
            Func::Set(material->GetUniformLocation(name, true), args...);
        }
    }
}