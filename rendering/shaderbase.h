#ifndef _VENGINE_RENDERING_SHADERBASE_
#define _VENGINE_RENDERING_SHADERBASE_

#include <GL/glew.h>
#include <string>
#include <vector>
#include "../resources/rwopsresource.h"
#include "../pointer.h"

class Material;

class ShaderBase : public RWopsResource
{
protected:
    bool compiled = false;
    GLuint shaderID;
    GLenum type;
    std::vector<Material*> usedIn = {};

    void Compile(SDL_RWops *src, GLenum type);

public:
    ShaderBase(SDL_RWops *src, GLenum type);
    ~ShaderBase();
    bool IsValid() const override;
    GLuint GetShaderID() const;
    virtual GLenum GetShaderType() const = 0;
    std::string GetShaderLog() const;
    static const char *GetShaderTypeName(GLenum type);
    void OnSourceChange(SDL_RWops *src) override;

    friend class Material;
};

#endif