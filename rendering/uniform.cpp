#include "uniform.h"

void glUniform1fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniform1fv(location, count, value);
}

void glUniform2fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniform2fv(location, count, value);
}

void glUniform3fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniform3fv(location, count, value);
}

void glUniform4fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniform4fv(location, count, value);
}

void glUniform1ivconst(GLint location, GLsizei count, const GLint *value)
{
    glUniform1iv(location, count, value);
}

void glUniform2ivconst(GLint location, GLsizei count, const GLint *value)
{
    glUniform2iv(location, count, value);
}

void glUniform3ivconst(GLint location, GLsizei count, const GLint *value)
{
    glUniform3iv(location, count, value);
}

void glUniform4ivconst(GLint location, GLsizei count, const GLint *value)
{
    glUniform4iv(location, count, value);
}

void glUniform1uivconst(GLint location, GLsizei count, const GLuint *value)
{
    glUniform1uiv(location, count, value);
}

void glUniform2uivconst(GLint location, GLsizei count, const GLuint *value)
{
    glUniform2uiv(location, count, value);
}

void glUniform3uivconst(GLint location, GLsizei count, const GLuint *value)
{
    glUniform3uiv(location, count, value);
}

void glUniform4uivconst(GLint location, GLsizei count, const GLuint *value)
{
    glUniform4uiv(location, count, value);
}

void glUniformMatrix2fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix2fv(location, count, GL_FALSE, value);
}

void glUniformMatrix3fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix3fv(location, count, GL_FALSE, value);
}

void glUniformMatrix4fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix4fv(location, count, GL_FALSE, value);
}

void glUniformMatrix2x3fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix2x3fv(location, count, GL_FALSE, value);
}

void glUniformMatrix3x2fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix3x2fv(location, count, GL_FALSE, value);
}

void glUniformMatrix2x4fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix2x4fv(location, count, GL_FALSE, value);
}

void glUniformMatrix4x2fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix4x2fv(location, count, GL_FALSE, value);
}

void glUniformMatrix3x4fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix3x4fv(location, count, GL_FALSE, value);
}

void glUniformMatrix4x3fvconst(GLint location, GLsizei count, const GLfloat *value)
{
    glUniformMatrix4x3fv(location, count, GL_FALSE, value);
}
