#include "renderer.h"
#include "../ui/uimanager.h"
#include "components/linerenderer.h"
#include <stdexcept>
#include <format>

bool Renderer::Start()
{
    SDL_Init(SDL_INIT_VIDEO);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);
    if (!(window = SDL_CreateWindow(windowName, 0, 0, resolution.x, resolution.y, SDL_WINDOW_OPENGL)))
    {
        SDL_LogError(0, SDL_GetError());
        return false;
    }
    if (!(context = SDL_GL_CreateContext(window)))
    {
        SDL_LogError(0, SDL_GetError());
        return false;
    }
    glEnable(GL_MULTISAMPLE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK)
    {
        SDL_LogError(0, (char *)glewGetErrorString(glewError));
        return false;
    }
    if (SDL_GL_SetSwapInterval(1))
    {
        SDL_LogWarn(0, SDL_GetError());
    }
    SetResolution({1280, 720});
    UIManager::Start();
    return true;
}

void Renderer::Stop()
{
    UIManager::Stop();
    if (context)
    {
        SDL_GL_DeleteContext(context);
    }
    if (window)
    {
        SDL_DestroyWindow(window);
    }
}

void Renderer::Update()
{
    glClear(GL_COLOR_BUFFER_BIT);
    LineRenderer::RenderAll();
    UIManager::Draw();
    SDL_GL_SwapWindow(window);
}

SDL_Window *Renderer::GetWindow()
{
    return window;
}

Vector2Int Renderer::GetResolution()
{
    return resolution;
}

void Renderer::SetResolution(const Vector2Int &resolution)
{
    Renderer::resolution = resolution;
    SDL_SetWindowSize(window, resolution.x, resolution.y);
    glViewport(0, 0, resolution.x, resolution.y);
    Material::SetGlobalUniform<Uniform2i>("resolution", resolution.x, resolution.y);
}