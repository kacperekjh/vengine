#include "camera.h"
#include "../../structs/vector2.h"
#include "../renderer.h"
#include "../../gameobjects/objectmanager.h"
#include "../material.h"
#include <glm/mat4x4.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void Camera::UpdateProjectionMatrix()
{
    if (!GetOwner()->IsOnScene() || !IsInstance())
    {
        return;
    }
    Vector2Int resolution = Renderer::GetResolution();
    float aspectRatio = (float)resolution.x / resolution.y;
    glm::mat4x4 projection;
    if (Camera::projection == Projection::Perspective)
    {
        float fovY = 2 * atan(tan(fovX / 2) / aspectRatio);
        projection = glm::perspective(fovY, aspectRatio, nearClipPlane, farClipPlane);
    }
    else
    {
        float hWidth = orthographicWidth / 2;
        float hHeight = hWidth / aspectRatio;
        projection = glm::orthoRH_NO(-hWidth, hWidth, -hHeight, hHeight, nearClipPlane, farClipPlane);
    }
    Material::SetGlobalUniform<UniformMatrix4x4>("projectionMatrix", glm::value_ptr(projection));
    Material::SetGlobalUniform<Uniform1f>("nearClip", nearClipPlane);
    Material::SetGlobalUniform<Uniform1f>("farClip", farClipPlane);
}

void Camera::UpdateModelViewMatrix()
{
    if (!GetOwner()->IsOnScene() || !IsInstance())
    {
        return;
    }
    Vector3 negPosition = -GetOwner()->position;
    Vector3 negRotation = -GetOwner()->rotation;
    glm::mat4x4 modelView = glm::translate(glm::mat4(1.f), glm::vec3(negPosition.x, negPosition.y, negPosition.z));
    modelView = glm::rotate(modelView, negRotation.x, glm::vec3(1, 0, 0));
    modelView = glm::rotate(modelView, negRotation.y, glm::vec3(0, -1, 0));
    modelView = glm::rotate(modelView, negRotation.z, glm::vec3(0, 0, 1));
    Material::SetGlobalUniform<UniformMatrix4x4>("modelViewMatrix", glm::value_ptr(modelView));
}

void Camera::Awake()
{
    if (!GetInstance(GetScene()))
    {
        SetInstance();
    }
}

void Camera::Start()
{
    UpdateProjectionMatrix();
}

void Camera::LateUpdate()
{
    if (IsInstance())
    {
        UpdateModelViewMatrix();
    }
}

void Camera::OnSceneLoaded()
{
    UpdateProjectionMatrix();
    UpdateModelViewMatrix();
}

float Camera::GetFOV() const
{
    return fovX;
}

void Camera::SetFOV(float fov)
{
    fovX = fov;
    if (projection == Projection::Perspective)
    {
        UpdateProjectionMatrix();
    }
}

void Camera::MakeMain()
{
    SetInstance(true);
}

void Camera::SetProjection(Projection projection)
{
    Camera::projection = projection;
    UpdateProjectionMatrix();
}

Projection Camera::GetProjection() const
{
    return projection;
}

float Camera::GetNearClipPlane() const
{
    return nearClipPlane;
}

void Camera::SetNearClipPlane(float distance)
{
    nearClipPlane = distance;
    UpdateProjectionMatrix();
}

float Camera::GetFarClipPlane() const
{
    return farClipPlane;
}

void Camera::SetFarClipPlane(float distance)
{
    farClipPlane = distance;
    UpdateProjectionMatrix();
}

float Camera::GetOrthographicWidth() const
{
    return orthographicWidth;
}

void Camera::SetOrthographicWidth(float width)
{
    orthographicWidth = width;
    if (projection == Projection::Orthographic)
    {
        UpdateProjectionMatrix();
    }
}