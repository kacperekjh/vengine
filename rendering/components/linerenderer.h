#ifndef _VENGINE_RENDERING_COMPONENTS_LINERENDERER_
#define _VENGINE_RENDERING_COMPONENTS_LINERENDERER_

#include "../../gameobjects/component.h"
#include "../../components/linetransform.h"
#include <map>

/// @brief Component used to render objects
class LineRenderer : public Component
{
private:
    static inline std::multimap<int, Pointer<LineRenderer>> queue = {};

    std::vector<Vector3> vertices;
    std::vector<GLuint> indecies;
    size_t oldVertexSize = 0;
    std::vector<size_t> lines;
    GLuint vbo;
    GLuint ibo;
    GLuint vao;
    Pointer<LineTransform> transform = nullptr;
    unsigned transformID = 0;
    Pointer<Material> material = nullptr;
    int priority = 0;
    
    void AddLine(const Line3D &line);

    static void RenderAll();
    void Render();

protected:
    void LateUpdate() override;

public:
    LineRenderer();
    ~LineRenderer();
    /// @brief Changes the material of this object.
    void SetMaterial(const Pointer<Material> &material);
    /// @brief Returns the material that's used by this object.
    const Pointer<Material> &GetMaterial();
    /// @brief Changes the priority of the renderer. Lines with higher priority will be drawn
    ///        on top of lines with lower priority.
    void SetPriority(int priority);
    /// @brief Returns the priority of the renderer.
    int GetPriority() const;

    friend class Renderer;
};

#endif