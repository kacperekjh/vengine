#ifndef _VENGINE_RENDERING_COMPONENTS_CAMERA_
#define _VENGINE_RENDERING_COMPONENTS_CAMERA_

#include "../../gameobjects/component.h"
#include <SDL2/SDL.h>
#include "../../math.h"
#include "../../mixins/scenesingleton.h"

/// @brief Projection type
enum Projection
{
    Orthographic,
    Perspective
};

/// @brief Component used for rendering
class Camera : public Component, public SceneSingleton<Camera, false>
{
private:
    float fovX = 70_deg;
    Projection projection = Projection::Orthographic;
    float orthographicWidth = 10;
    float nearClipPlane = 0.1f;
    float farClipPlane = 100.f;

    void UpdateProjectionMatrix();
    void UpdateModelViewMatrix();

protected:
    void Awake() override;
    void Start() override;
    void LateUpdate() override;
    void OnSceneLoaded() override;

public: 
    /// @brief Returns the current field of view
    float GetFOV() const;
    /// @brief Changes the field of view
    /// @param fov field of view in radians
    void SetFOV(float fov);
    /// @brief Make this camera the main camera of the current scene.
    void MakeMain();
    /// @brief Changes the projection type used by the camera.
    void SetProjection(Projection projection);
    /// @brief Returns the projection type used by the camera.
    Projection GetProjection() const;
    /// @brief Returns the distance of the near clip plane.
    float GetNearClipPlane() const;
    /// @brief Sets the distance of the near clip plane.
    void SetNearClipPlane(float distance);
    /// @brief Returns the distance of the far clip plane.
    float GetFarClipPlane() const;
    /// @brief Sets the distance of the far clip plane.
    void SetFarClipPlane(float distance);
    /// @brief Returns the width of orthographic projection view.
    float GetOrthographicWidth() const;
    /// @brief Sets the width of orthographic projection view.
    void SetOrthographicWidth(float width);
};

#endif