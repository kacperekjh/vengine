#include "linerenderer.h"
#include "camera.h"
#include "../renderer.h"

void LineRenderer::AddLine(const Line3D &line)
{
    const auto &points = line.GetPoints();
    vertices.reserve(vertices.size() + points.size());
    indecies.reserve(indecies.size() + points.size());
    for (GLuint i = 0; i < points.size(); i++)
    {
        indecies.push_back(vertices.size() + i);
    }
    vertices.insert(vertices.end(), points.begin(), points.end());
    lines.push_back(line.GetPoints().size());
}

void LineRenderer::RenderAll()
{
    for (auto &[priority, line] : queue)
    {
        if (line)
        {
            line->Render();
        }
    }
    queue.clear();
}

void LineRenderer::Render()
{
    if (!material || !material->IsValid())
    {
        return;
    }
    
    glUseProgram(material->GetProgramID());
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    if (oldVertexSize == vertices.size())
    {
        glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(float) * 3, vertices.data());
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indecies.size() * sizeof(GLuint), indecies.data());
    }
    else
    {
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) * 3, vertices.data(), GL_STATIC_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indecies.size() * sizeof(float), indecies.data(), GL_STATIC_DRAW);
        oldVertexSize = vertices.size();
    }
    size_t start = 0;
    for (size_t length : lines)
    {
        glDrawArrays(GL_LINE_STRIP, start, length);
        start += length;
    }
    glBindVertexArray(0);
    glUseProgram(0);
}

void LineRenderer::LateUpdate()
{
    if (!transform)
    {
        transform = GetOwner()->GetComponent<LineTransform>();
    }
    if (transform && transformID != transform->GetTransformID())
    {
        vertices.clear();
        indecies.clear();
        lines.clear();
        for (const auto &line : transform->GetTransformed())
        {
            AddLine(line);
        }
        transformID = transform->GetTransformID();
    }
    queue.insert({priority, GetPointer().StaticCast<LineRenderer>()});
}

LineRenderer::LineRenderer()
{
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo);
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, nullptr);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
}

LineRenderer::~LineRenderer()
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &ibo);
}

void LineRenderer::SetMaterial(const Pointer<Material> &material)
{
    LineRenderer::material = material;
}

const Pointer<Material> &LineRenderer::GetMaterial()
{
    return material;
}

void LineRenderer::SetPriority(int priority)
{
    LineRenderer::priority = priority;
}

int LineRenderer::GetPriority() const
{
    return priority;
}