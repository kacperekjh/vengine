#ifndef _VENGINE_RENDERING_SHADER_
#define _VENGINE_RENDERING_SHADER_

#include "shaderbase.h"

/// @brief Resource that compiles and manages an OpenGL shader.
template <GLenum ShaderType>
class Shader : public ShaderBase
{
public:
    static_assert(ShaderType == GL_VERTEX_SHADER ||
                  ShaderType == GL_TESS_CONTROL_SHADER ||
                  ShaderType == GL_TESS_EVALUATION_SHADER ||
                  ShaderType == GL_GEOMETRY_SHADER ||
                  ShaderType == GL_FRAGMENT_SHADER ||
                  ShaderType == GL_COMPUTE_SHADER,
                  "Invalid shader type.");

    Shader(SDL_RWops *src);
    GLenum GetShaderType() const override;
};

#include "shader.hxx"

using VertexShader = Shader<GL_VERTEX_SHADER>;
using TessControlShader = Shader<GL_TESS_CONTROL_SHADER>;
using TessEvaluationShader = Shader<GL_TESS_EVALUATION_SHADER>;
using GeometryShader = Shader<GL_GEOMETRY_SHADER>;
using FragmentShader = Shader<GL_FRAGMENT_SHADER>;
using ComputeShader = Shader<GL_COMPUTE_SHADER>;

#endif