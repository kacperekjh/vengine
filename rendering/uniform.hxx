template <class T, size_t Count, void(*glUniform)(GLint, GLsizei, const T*)>
template <std::same_as<T>... Args>
Uniform<T, Count, glUniform>::RawOutputType Uniform<T, Count, glUniform>::Set(GLint location, Args... args) requires Equal<sizeof...(Args), Count>
{
    auto value = MakeRaw(args...);
    glUniform(location, 1, (const T *)value.data());
    return value;
}

template <class T, size_t Count, void(*glUniform)(GLint, GLsizei, const T*)>
Uniform<T, Count, glUniform>::RawOutputType Uniform<T, Count, glUniform>::Set(GLint location, const T *value)
{
    glUniform(location, 1, value);
    return MakeRaw(value);
}

template <class T, size_t Count, void(*glUniform)(GLint, GLsizei, const T*)>
Uniform<T, Count, glUniform>::RawOutputType Uniform<T, Count, glUniform>::Set(GLint location, const std::span<uint8_t> value)
{
    if (value.size() != sizeof(T) * Count)
    {
        throw std::invalid_argument("Invalid size of raw data.");
    }
    glUniform(location, 1, (const T *)value.data());
    return MakeRaw(value);
}

template <class T, size_t Count, void (*glUniform)(GLint, GLsizei, const T *)>
template <std::same_as<T>... Args>
Uniform<T, Count, glUniform>::RawOutputType Uniform<T, Count, glUniform>::MakeRaw(Args... args) requires Equal<sizeof...(Args), Count>
{
    std::array<T, Count> value = {args...};
    RawOutputType out = {};
    out.resize(sizeof(T) * Count);
    memcpy(out.data(), value.data(), Count * sizeof(T));
    return out;
}

template <class T, size_t Count, void(*glUniform)(GLint, GLsizei, const T*)>
Uniform<T, Count, glUniform>::RawOutputType Uniform<T, Count, glUniform>::MakeRaw(const T *value)
{
    RawOutputType out = {};
    out.resize(sizeof(T) * Count);
    memcpy(out.data(), value, sizeof(T) * Count);
    return out;
}

template <class T, size_t Count, void(*glUniform)(GLint, GLsizei, const T*)>
Uniform<T, Count, glUniform>::RawOutputType Uniform<T, Count, glUniform>::MakeRaw(const std::span<uint8_t> value)
{
    if (value.size() != sizeof(T) * Count)
    {
        throw std::invalid_argument("Invalid size of raw data.");
    }
    RawOutputType out = {};
    out.resize(sizeof(T) * Count);
    memcpy(out.data(), value.data(), Count * sizeof(T));
    return out;
}
