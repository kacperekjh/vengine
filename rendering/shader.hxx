template <GLenum Type>
Shader<Type>::Shader(SDL_RWops *src) : ShaderBase(src, Type) {}

template <GLenum Type>
GLenum Shader<Type>::GetShaderType() const
{
    return Type;
}