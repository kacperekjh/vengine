#ifndef _VENGINE_RENDERING_TEXTURE2D_
#define _VENGINE_RENDERING_TEXTURE2D_

#include <GL/glew.h>
#include <GL/gl.h>
#include <SDL2/SDL.h>
#include "../resources/resource.h"

class Texture2D : public Resource
{
private:
    GLuint textureID = 0;

public:
    Texture2D();
    ~Texture2D();
    bool IsValid() const override;
    GLuint GetTextureID() const;
    void Bind(GLenum textureUnit = GL_TEXTURE0) const;
    void Unbind() const;
    void SetData(GLsizei width, GLsizei height, GLenum internalFormat, GLenum sourceFormat, const void *data);
    void SetData(SDL_Surface *surface);
    void SetParameteri(GLenum name, GLint value);
    void SetParameterf(GLenum name, GLfloat value);
};

#endif