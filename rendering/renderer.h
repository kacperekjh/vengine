#ifndef _VENGINE_RENDERING_RENDERER_
#define _VENGINE_RENDERING_RENDERER_

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <GL/glu.h>
#include "../structs/vector2int.h"
#include "../structs/vector2.h"

class Renderer
{
private:
    static inline SDL_Window *window;
    static inline SDL_GLContext context;
    static inline Vector2Int resolution;

    static bool Start();
    static void Stop();
    static void Update();

    static uint8_t CalculateOutcode(Vector2 v);

public:
    static inline const char *windowName = "VEngine";

    /// @brief Returns a pointer to the main SDL_Window
    static SDL_Window *GetWindow();
    /// @brief Returns resolution of the window.
    static Vector2Int GetResolution();
    /// @brief Changes resolution of the window.
    static void SetResolution(const Vector2Int &resolution);

    friend class GameLoop;
};

#endif