#ifndef _VENGINE_RENDERING_UNIFORMS_UNIFORMV_
#define _VENGINE_RENDERING_UNIFORMS_UNIFORMV_

#include <vector>
#include <cstddef>
#include <concepts>
#include <GL/glew.h>
#include <GL/gl.h>
#include <span>
#include <stdexcept>
#include <cstring>

//
// I apologize for making this unholy abomination. I sincerely hope no one will have to read this.
// I actually had a lot of fun writing this.
//

// I had to make this awful block because I couldn't find a way to have const pointers to glUniform* functions.
void glUniform1fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniform2fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniform3fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniform4fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniform1ivconst(GLint location, GLsizei count, const GLint *value);
void glUniform2ivconst(GLint location, GLsizei count, const GLint *value);
void glUniform3ivconst(GLint location, GLsizei count, const GLint *value);
void glUniform4ivconst(GLint location, GLsizei count, const GLint *value);
void glUniform1uivconst(GLint location, GLsizei count, const GLuint *value);
void glUniform2uivconst(GLint location, GLsizei count, const GLuint *value);
void glUniform3uivconst(GLint location, GLsizei count, const GLuint *value);
void glUniform4uivconst(GLint location, GLsizei count, const GLuint *value);
// Let's assume we'll never have to deal with matrices that need to be transposed :3
void glUniformMatrix2fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix3fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix4fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix2x3fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix3x2fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix2x4fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix4x2fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix3x4fvconst(GLint location, GLsizei count, const GLfloat *value);
void glUniformMatrix4x3fvconst(GLint location, GLsizei count, const GLfloat *value);

template <unsigned A, unsigned B>
concept Equal = A == B;

template <class T, size_t Count, void(*glUniform)(GLint, GLsizei, const T*)>
struct Uniform
{
    using RawOutputType = std::vector<uint8_t>;

    template <std::same_as<T>... Args>
    static RawOutputType Set(GLint location, Args ...args) requires Equal<sizeof...(Args), Count>;
    static RawOutputType Set(GLint location, const T *value);
    static RawOutputType Set(GLint location, const std::span<uint8_t> value);
    template <std::same_as<T>... Args>
    static RawOutputType MakeRaw(Args... args) requires Equal<sizeof...(Args), Count>;
    static RawOutputType MakeRaw(const T *value);
    static RawOutputType MakeRaw(const std::span<uint8_t> value);
};

#include "uniform.hxx"

using Uniform1f = Uniform<GLfloat, 1, glUniform1fvconst>;
using Uniform2f = Uniform<GLfloat, 2, glUniform2fvconst>;
using Uniform3f = Uniform<GLfloat, 3, glUniform3fvconst>;
using Uniform4f = Uniform<GLfloat, 4, glUniform4fvconst>;
using Uniform1i = Uniform<GLint, 1, glUniform1ivconst>;
using Uniform2i = Uniform<GLint, 2, glUniform2ivconst>;
using Uniform3i = Uniform<GLint, 3, glUniform3ivconst>;
using Uniform4i = Uniform<GLint, 4, glUniform4ivconst>;
using Uniform1ui = Uniform<GLuint, 1, glUniform1uivconst>;
using Uniform2ui = Uniform<GLuint, 2, glUniform2uivconst>;
using Uniform3ui = Uniform<GLuint, 3, glUniform3uivconst>;
using Uniform4ui = Uniform<GLuint, 4, glUniform4uivconst>;
using UniformMatrix2x2 = Uniform<GLfloat, 2 * 2, glUniformMatrix2fvconst>;
using UniformMatrix3x3 = Uniform<GLfloat, 3 * 3, glUniformMatrix3fvconst>;
using UniformMatrix4x4 = Uniform<GLfloat, 4 * 4, glUniformMatrix4fvconst>;
using UniformMatrix2x3 = Uniform<GLfloat, 2 * 3, glUniformMatrix2x3fvconst>;
using UniformMatrix3x2 = Uniform<GLfloat, 3 * 2, glUniformMatrix3x2fvconst>;
using UniformMatrix2x4 = Uniform<GLfloat, 2 * 4, glUniformMatrix2x4fvconst>;
using UniformMatrix4x2 = Uniform<GLfloat, 4 * 2, glUniformMatrix4x2fvconst>;
using UniformMatrix3x4 = Uniform<GLfloat, 3 * 4, glUniformMatrix3x4fvconst>;
using UniformMatrix4x3 = Uniform<GLfloat, 4 * 3, glUniformMatrix4x3fvconst>;

#endif