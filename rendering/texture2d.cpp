#include "texture2d.h"

Texture2D::Texture2D()
{
    glGenTextures(1, &textureID);
    SetParameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    SetParameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

Texture2D::~Texture2D()
{
    glDeleteTextures(1, &textureID);
}

bool Texture2D::IsValid() const
{
    return textureID != 0;
}

GLuint Texture2D::GetTextureID() const
{
    return textureID;
}

void Texture2D::Bind(GLenum textureUnit) const
{
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_2D, textureID);
}

void Texture2D::Unbind() const
{
    glActiveTexture(0);
}

void Texture2D::SetData(GLsizei width, GLsizei height, GLenum internalFormat, GLenum sourceFormat, const void *data)
{
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, sourceFormat, GL_UNSIGNED_BYTE, data);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::SetData(SDL_Surface *surface)
{
    SDL_Surface *temp = SDL_CreateRGBSurfaceWithFormat(0, surface->w, surface->h, 32, SDL_PIXELFORMAT_RGBA32);
    SDL_BlitSurface(surface, 0, temp, 0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, temp->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);
    SDL_FreeSurface(temp);
}

void Texture2D::SetParameteri(GLenum name, GLint value)
{
    Bind();
    glTexParameteri(GL_TEXTURE_2D, name, value);
    Unbind();
}

void Texture2D::SetParameterf(GLenum name, GLfloat value)
{
    Bind();
    glTexParameterf(GL_TEXTURE_2D, name, value);
    Unbind();
}