#ifndef _VENGINE_RENDERING_MATERIAL_
#define _VENGINE_RENDERING_MATERIAL_

#include "../resources/resource.h"
#include "shader.h"
#include "../pointer.h"
#include <map>
#include <vector>
#include <glm/mat4x4.hpp>
#include "uniform.h"
#include <type_traits>

template <class Func, class ...Args>
concept ValidUniform = requires(GLint location, Args ...args, const std::span<uint8_t> data)
{
    { Func::Set(location, data) } -> std::convertible_to<std::vector<uint8_t>>;
    { Func::Set(location, args...) } -> std::convertible_to<std::vector<uint8_t>>;
    { Func::MakeRaw(args...) } -> std::convertible_to<std::vector<uint8_t>>;
};

/// @brief Resource that manages an OpenGL shader program.
class Material : public Resource
{
private:
    struct Field
    {
        std::vector<uint8_t> data;
        std::vector<uint8_t> (*setter)(GLint, const std::span<uint8_t>);
        bool silent;
    };

    static inline std::vector<Material *> materials = {};
    static inline std::map<std::string, Field> globalFields = {};
    std::map<GLenum, Pointer<ShaderBase>> shaders;
    bool linked = false;
    GLuint programID = 0;
    std::map<std::string, Field> fields;

    GLint GetUniformLocation(std::string_view name, bool silent) const;

public:
    Material();
    ~Material();
    /// @brief Assigns a new shader to this material. `Link()` has to be called to actually use the shader.
    ///        Assigning multiple shaders of the same type will cause them to get replaced.
    void SetShader(Pointer<ShaderBase> shader);
    /// @brief Links the shader program. Can be called multiple times.
    bool Link(bool clearFields = false);
    /// @brief Returns the ID of the shader program or 0 when not linked.
    GLuint GetProgramID() const;
    /// @brief Changes value of a uniform variable named `name` by calling `Func::Set(location, args...)`. Can be used before linking.
    template <class Func, class ...Args>
    void SetUniform(std::string_view name, Args ...args) requires(ValidUniform<Func, Args...>);
    /// @brief Tries to changes value of a uniform variable named `name` by calling `Func::Set(location, args...)`. Can be used before linking.
    template <class Func, class ...Args>
    void TrySetUniform(std::string_view name, Args ...args) requires(ValidUniform<Func, Args...>);
    /// @brief Returns if the program has been linked successfully.
    bool IsValid() const override;
    /// @brief Returns the program log from `glGetProgramInfoLog`.
    std::string GetProgramLog() const;
    /// @brief Changes value of a uniform variable named `name` in every material. Automatically changes the variable on materials
    ///        that get created after calling this function.
    template <class Func, class ...Args>
    static void SetGlobalUniform(std::string_view name, Args ...args) requires(ValidUniform<Func, Args...>);
    /// @brief Clones this material.
    Pointer<Material> Clone() const;
};

#include "material.hxx"

#endif