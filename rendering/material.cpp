#include "material.h"
#include <algorithm>
#include <glm/gtc/type_ptr.hpp>
#include "../resources/resourcemanager.h"

GLint Material::GetUniformLocation(std::string_view name, bool silent) const
{
    if (!linked)
    {
        return -1;
    }
    glUseProgram(programID);
    GLint location = glGetUniformLocation(programID, name.data());
    if (location == -1 && !silent)
    {
        SDL_LogWarn(0, "Uniform '%s' doesn't exist.", name.data());
    }
    return location;
}

Material::Material()
{
    materials.push_back(this);
}

Material::~Material()
{
    if (programID != 0)
    {
        glDeleteProgram(programID);
    }
    materials.erase(std::find(materials.begin(), materials.end(), this));
}

void Material::SetShader(Pointer<ShaderBase> shader)
{
    auto it = shaders.find(shader->GetShaderType());
    if (it != shaders.end())
    {
        auto &usedIn = it->second->usedIn;
        usedIn.erase(std::find(usedIn.begin(), usedIn.end(), this));
    }
    shader->usedIn.push_back(this);
    shaders[shader->GetShaderType()] = shader;
}

bool Material::Link(bool clearFields)
{
    if (clearFields)
    {
        fields.clear();
    }
    if (programID != 0)
    {
        glDeleteProgram(programID);
    }
    programID = glCreateProgram();
    for (auto &shader : shaders)
    {
        if (!shader.second->IsValid())
        {
            SDL_LogWarn(0, "Passed invalid or missing shader. Ignoring.");
            continue;
        }
        glAttachShader(programID, shader.second->GetShaderID());
    }
    glLinkProgram(programID);
    GLint programSuccess = GL_TRUE;
    glGetProgramiv(programID, GL_LINK_STATUS, &programSuccess);
    if (!programSuccess)
    {
        SDL_LogError(0, "Failed to link program. %s", GetProgramLog().c_str());
        return false;
    }
    linked = true;
    for (auto &field : globalFields)
    {
        GLint location = GetUniformLocation(field.first, true);
        if (location != -1)
        {
            field.second.setter(location, field.second.data);
        }
    }
    for (auto &field : fields)
    {
        GLint location = GetUniformLocation(field.first, field.second.silent);
        if (location != -1)
        {
            field.second.setter(location, field.second.data);
        }
    }
    return true;
}

GLuint Material::GetProgramID() const
{
    return programID;
}

bool Material::IsValid() const
{
    return linked;
}

std::string Material::GetProgramLog() const
{
    if (programID == 0)
    {
        SDL_LogWarn(0, "Cannot get log of program that hasn't been created yet.");
        return "";
    }
    int infoLogLength = 0;
    int maxLength = 0;
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);
    char *infoLog = new char[maxLength];
    glGetProgramInfoLog(programID, maxLength, &infoLogLength, infoLog);
    std::string log = infoLog;
    delete[] infoLog;
    return log;
}

Pointer<Material> Material::Clone() const
{
    auto copy = ResourceManager::CreateResource<Material>();
    copy->shaders = shaders;
    copy->fields = fields;
    if (linked)
    {
        copy->Link();
    }
    for (auto &shader : shaders)
    {
        shader.second->usedIn.push_back((Material*)copy);
    }
    return copy;
}