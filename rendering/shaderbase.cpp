#include "shaderbase.h"
#include "renderer.h"
#include "material.h"

void ShaderBase::Compile(SDL_RWops *src, GLenum type)
{
    if (!src)
    {
        SDL_LogWarn(0, "Passed null pointer to shader resource.");
        return;
    }
    char *data = new char[SDL_RWsize(src) + 1];
    SDL_RWread(src, data, SDL_RWsize(src), 1);
    data[SDL_RWsize(src)] = '\0';
    GLchar *sources[1] = {data};
    shaderID = glCreateShader(type);
    glShaderSource(shaderID, 1, sources, nullptr);
    glCompileShader(shaderID);
    GLint shaderCompiled = GL_FALSE;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &shaderCompiled);
    if (shaderCompiled != GL_TRUE)
    {
        SDL_LogError(0, "Failed to compile %s shader. %s", GetShaderTypeName(type), GetShaderLog().c_str());
        return;
    }
    delete[] data;
    compiled = true;
}

ShaderBase::ShaderBase(SDL_RWops *src, GLenum type) : RWopsResource(src), type(type)
{
    Compile(src, type);
}

ShaderBase::~ShaderBase()
{
    if (shaderID != 0)
    {
        glDeleteShader(shaderID);
    }
}

bool ShaderBase::IsValid() const
{
    return compiled;
}

GLuint ShaderBase::GetShaderID() const
{
    return shaderID;
}

std::string ShaderBase::GetShaderLog() const
{
    int infoLogLength = 0;
    int maxLength = 0;
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);
    char *infoLog = new char[maxLength];
    glGetShaderInfoLog(shaderID, maxLength, &infoLogLength, infoLog);
    std::string log = infoLog;
    delete[] infoLog;
    return log;
}

const char *ShaderBase::GetShaderTypeName(GLenum type)
{
    switch (type)
    {
    case GL_VERTEX_SHADER:
        return "vertex";
    case GL_TESS_CONTROL_SHADER:
        return "tessellation control";
    case GL_TESS_EVALUATION_SHADER:
        return "tesselation evaluation";
    case GL_GEOMETRY_SHADER:
        return "geometry";
    case GL_FRAGMENT_SHADER:
        return "fragment";
    case GL_COMPUTE_SHADER:
        return "compute";
    default:
        return "<invalid type>";
    }
}

void ShaderBase::OnSourceChange(SDL_RWops *src)
{
    compiled = false;
    Compile(src, type);
    for (auto &material : usedIn)
    {
        material->Link();
    }
}