#include "gameobject.h"
#include "objectmanager.h"
#include <algorithm>

void GameObject::Update()
{
    components.erase(std::remove_if(components.begin(), components.end(), [](auto& comp)
    { 
        return !comp.GetState(); 
    }), components.end());
    for (auto &component : components)
    {
        if (!component->started)
        {
            component->Start();
            component->started = true;
        }
        component->Update();
    }
}

void GameObject::LateUpdate()
{
    for (auto &component : components)
    {
        component->LateUpdate();
    }
}

void GameObject::OnDestroy()
{
    for (auto &component : components)
    {
        component->OnDestroy();
    }
}

void GameObject::OnSceneUnloaded()
{
    for (auto &component : components)
    {
        component->OnSceneUnloaded();
    }
}

void GameObject::OnSceneLoaded()
{
    for (auto &component : components)
    {
        component->OnSceneLoaded();
    }
}

GameObject::GameObject() {}

GameObject::~GameObject()
{
    for (auto &component : components)
    {
        component.Delete();
    }
    void Destroy();
}

void GameObject::Destroy()
{
    if (destroyed)
    {
        return;
    }
    ObjectManager::Destroy(GetPointer());
    destroyed = true;
}

const Pointer<Scene> &GameObject::GetScene() const
{
    return scene;
}

bool GameObject::IsOnScene() const
{
    return scene == ObjectManager::GetCurrentScene();
}