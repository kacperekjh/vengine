#include "component.h"
#include "gameobject.h"

Component::Component() {}

void Component::Awake() {}
void Component::Start() {}
void Component::Update() {}
void Component::LateUpdate() {}
void Component::OnDestroy() {}
void Component::OnSceneUnloaded() {}
void Component::OnSceneLoaded() {}

Component::~Component() {}

const Pointer<GameObject> &Component::GetOwner()
{
    return owner;
}

Pointer<const GameObject> Component::GetOwner() const
{
    return owner;
}

void Component::Destroy()
{
    OnDestroy();
    for (auto it = owner->components.begin(); it != owner->components.end(); ++it)
    {
        if (*it == GetPointer())
        {
            it->Delete();
            owner->components.erase(it);
            return;
        }
    }
}

const Pointer<Scene> &Component::GetScene() const
{
    return owner->GetScene();
}