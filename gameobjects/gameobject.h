#ifndef _VENGINE_GAMEOBJECTS_GAMEOBJECT_
#define _VENGINE_GAMEOBJECTS_GAMEOBJECT_

#include "../mixins/sharedpointer.h"
#include "../structs/vector3.h"
#include <vector>
#include <type_traits>

class Component;
class Scene;

#include "component.h"

template <class T>
concept ValidComponent = std::is_convertible_v<T*, Component*> && requires()
{
    new T();
};

/// @brief Class representing basic game objects.
class GameObject : public SharedPointer<GameObject>
{
private:
    Pointer<Scene> scene;
    std::vector<Pointer<Component>> components;
    bool destroyed = false;

    void Update();
    void LateUpdate();
    void OnDestroy();
    void OnSceneUnloaded();
    void OnSceneLoaded();
    GameObject();
    ~GameObject();

public:
    Vector3 position;
    Vector3 rotation;
    Vector3 scale = {1, 1, 1};

    /// @brief Creates a new component and adds it to the gameobject.
    /// @tparam T type of the component to add
    /// @param arguments arguments to pass to the component constructor
    /// @returns Pointer to the new component
    template <ValidComponent T>
    Pointer<T> AddComponent();
    /// @brief Searches and returns the first component with matching type in this gameobject.
    /// @tparam T type of the component to get
    /// @returns Pointer to the component or a null pointer
    template <class T>
    Pointer<T> GetComponent() const requires(std::is_convertible_v<T*, Component*>);
    /// @brief Destroyes this object at the end of the current frame.
    ///        Works exactly as `ObjectManager::Destroy(object)`
    void Destroy();
    /// @brief Returns the scene that the gameobject is on.
    const Pointer<Scene> &GetScene() const;
    /// @brief Returns if this object is on an active scene.
    bool IsOnScene() const;

    friend class ObjectManager;
    friend class Component;
    friend class Scene;
    friend class Renderer;
    friend class Pointer<GameObject>;
};

#include "gameobject.hxx"

#endif