#ifndef _VENGINE_GAMEOBJECTS_OBJECTMANAGER_
#define _VENGINE_GAMEOBJECTS_OBJECTMANAGER_

#include <vector>
#include <map>
#include <string>
#include <type_traits>
#include "component.h"
#include "gameobject.h"
#include "scene.h"

/// @brief Class that handles gameobjects
class ObjectManager
{
private:
    static inline std::vector<Pointer<GameObject>> gameObjects;
    static inline std::vector<Pointer<GameObject>> destroyQueue;
    static inline std::map<std::string, Pointer<Scene>> scenes;
    static inline Pointer<Scene> currentScene;
    static inline Pointer<Scene> nextScene;

    static bool Start();
    static void Update();
    static void Stop();

public:
    /// @brief Creates and new gameobject and returns its pointer.
    /// @param scene Scene on which the object will be created. Null for the currently selected scene.
    static const Pointer<GameObject> &CreateObject(const Pointer<Scene> &scene = nullptr);
    /// @brief Destroys a gameobject at the end of the current frame.
    ///        Works exactly as `object->Destroy()`
    /// @see GameObject::Destroy()
    /// @param object Gameobject to destroy
    static void Destroy(Pointer<GameObject> object);
    /// @brief Changes the current scene to `scene`.
    static void GoToScene(const Pointer<Scene> &scene);
    /// @brief Returns a pointer to a scene named `name` or null if it doesn't exist.
    static const Pointer<Scene> &GetScene(const std::string &name);
    /// @brief Returns pointer to the currently used scene or null.
    static const Pointer<Scene> &GetCurrentScene();
    /// @brief Creates a new scene identified by `name`.
    static const Pointer<Scene> &CreateScene(const std::string &name);

    friend class GameLoop;
};

#endif