#include "scene.h"
#include "objectmanager.h"

void Scene::OnLoad()
{
    if (uiRoot)
    {
        uiRoot->SetOnScene(true);
    }
    for (auto &object : objects)
    {
        object->OnSceneLoaded();
    }
}

void Scene::OnUnload()
{
    if (uiRoot)
    {
        uiRoot->SetOnScene(false);
    }
    for (auto &object : objects)
    {
        object->OnSceneUnloaded();
    }
}

Scene::Scene() {}

Scene::~Scene()
{
    for (auto object : objects)
    {
        ObjectManager::Destroy(object);
        
    }
    uiRoot.Delete();
}

const std::vector<Pointer<GameObject>> &Scene::GetObjects() const
{
    return objects;
}