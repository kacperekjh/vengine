#ifndef _VENGINE_GAMEOBJECTS_COMPONENT_
#define _VENGINE_GAMEOBJECTS_COMPONENT_

#include "../mixins/sharedpointer.h"

class GameObject;
class Scene;

/// @brief Class representing components used to add or extends GameObject behaviours.
class Component : public SharedPointer<Component>
{
private:
    Pointer<GameObject> owner;
    bool started = false;

protected:
    /// @brief Function called right after the component has been constructed and the scene has been set.
    virtual void Awake();
    /// @brief Function called before the first update.
    virtual void Start();
    /// @brief Function called on every frame.
    virtual void Update();
    /// @brief Function called on every frame after every other component has been updated.
    virtual void LateUpdate();
    /// @brief Function called before the component gets destroyed.
    virtual void OnDestroy();
    /// @brief Function called when the parent scene of this object gets unloaded.
    virtual void OnSceneUnloaded();
    /// @brief Function called when the parent scene of this object gets loaded.
    virtual void OnSceneLoaded();
    Component();
    virtual ~Component();

public:
    /// @brief Returns a pointer to the owner gameobject.
    const Pointer<GameObject> &GetOwner();
    /// @brief Returns a const pointer to the owner gameobject.
    Pointer<const GameObject> GetOwner() const;
    /// @brief Calls `OnDestroy()` then deletes the component.
    void Destroy();
    /// @brief Returns the scene that the parent gameobject is on.
    const Pointer<Scene> &GetScene() const;

    friend class GameObject;
    friend class Pointer<Component>;
};

#include "gameobject.h"
#include "scene.h"

#endif