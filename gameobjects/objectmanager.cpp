#include "objectmanager.h"
#include "../scenes.h"
#include <stdexcept>
#include <format>
#include <algorithm>

bool ObjectManager::Start()
{
    Scenes::CreateScenes();
    return true;
}

void ObjectManager::Update()
{
    if (currentScene != nextScene)
    {
        if (currentScene)
        {
            currentScene->OnUnload();
        }
        if (nextScene)
        {
            nextScene->OnLoad();
        }
        currentScene = nextScene;
    }
    if (!currentScene)
    {
        return;
    }
    auto &objects = currentScene->objects;
    objects.erase(std::remove_if(objects.begin(), objects.end(), [](const Pointer<GameObject> &o)
    {
        return !o.GetState();
    }), objects.end());
    for (auto &object : objects)
    {
        object->Update();
    }
    for (auto &object : currentScene->objects)
    {
        object->LateUpdate();
    }
    for (auto &object : destroyQueue)
    {
        object.Delete();
    }
    destroyQueue.clear();
}

void ObjectManager::Stop()
{
    for (auto &scene : scenes)
    {
        scene.second.Delete();
    }
}

const Pointer<GameObject> &ObjectManager::CreateObject(const Pointer<Scene> &scene)
{
    Pointer<Scene> sc = scene ? scene : currentScene;
    if (!sc)
    {
        throw std::runtime_error("Cannot create a gameobject without having a scene selected.");
    }
    auto object = new GameObject();
    auto ptr = object->GetPointer();
    ptr->scene = sc;
    sc->objects.push_back(ptr);
    return object->GetPointer();
}

void ObjectManager::Destroy(Pointer<GameObject> object)
{
    destroyQueue.push_back(object);
    object->OnDestroy();
}

void ObjectManager::GoToScene(const Pointer<Scene> &scene)
{
    nextScene = scene;
}

const Pointer<Scene> &ObjectManager::GetScene(const std::string &name)
{
    auto scene = scenes.find(name);
    if (scene == scenes.end())
    {
        return Pointer<Scene>::null;
    }
    else
    {
        return scene->second;
    }
}

const Pointer<Scene> &ObjectManager::GetCurrentScene()
{
    return currentScene;
}

const Pointer<Scene> &ObjectManager::CreateScene(const std::string &name)
{
    Scene *scene = new Scene();
    if (!scenes.insert({name, scene->GetPointer()}).second)
    {
        delete scene;
        throw std::runtime_error(std::format("Scene '{}' already exists.", name));
    }
    return scene->GetPointer();
}