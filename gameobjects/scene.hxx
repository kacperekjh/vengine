template <class T>
Pointer<T> Scene::GetInstance() const requires(std::is_convertible_v<T*, Component*>)
{
    auto index = std::type_index(typeid(T)); 
    auto it = instances.find(index);
    if (it != instances.end())
    {
        return it->second.DynamicCast<T>();
    }
    return nullptr;
}

template <class T>
void Scene::SetInstance(const Pointer<T> &component) requires(std::is_convertible_v<T*, Component*>)
{
    auto index = std::type_index(typeid(T));
    instances[index] = component;
}