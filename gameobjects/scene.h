#ifndef _VENGINE_GAMEOBJECTS_SCENE_
#define _VENGINE_GAMEOBJECTS_SCENE_

#include <vector>
#include <type_traits>
#include <typeindex>
#include <map>
#include "../ui/uielement.h"
#include "../mixins/sharedpointer.h"

class GameObject;
class Component;

#include "gameobject.h"

/// @brief Class representing a single in-game scene containg gameobjects.
class Scene : public SharedPointer<Scene>
{
private:
    std::vector<Pointer<GameObject>> objects = {};
    std::map<std::type_index, Pointer<Component>> instances = {};

    void OnLoad();
    void OnUnload();
    Scene();

public:
    Pointer<UIElement> uiRoot = {};

    ~Scene();
    /// @brief Returns the per-scene instance of component `T`.
    template <class T>
    Pointer<T> GetInstance() const requires(std::is_convertible_v<T*, Component*>);
    /// @brief Sets the per-scene instance of component `T`.
    template <class T>
    void SetInstance(const Pointer<T> &component) requires(std::is_convertible_v<T*, Component*>);
    /// @brief Returns all objects on this scene.
    const std::vector<Pointer<GameObject>> &GetObjects() const;
    
    friend class ObjectManager;
};

#include "scene.hxx"

#endif 