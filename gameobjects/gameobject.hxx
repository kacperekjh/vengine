template <ValidComponent T>
Pointer<T> GameObject::AddComponent()
{
    Component *instance = new T();
    instance->owner = GetPointer();
    components.push_back(instance->GetPointer());
    instance->Awake();
    return instance->GetPointer().DynamicCast<T>();
}

template <class T>
Pointer<T> GameObject::GetComponent() const requires(std::is_convertible_v<T*, Component*>)
{
    for (auto &component : components)
    {
        auto casted = component.DynamicCast<T>();
        if (casted)
        {
            return casted;
        }
    }
    return Pointer<Component>().DynamicCast<T>();
}