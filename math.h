#ifndef _VENGINE_MATH_
#define _VENGINE_MATH_

#include <cmath>

/// @brief Converts `deg` to radians.
constexpr long double operator""_deg(long double deg)
{
    return deg * M_PI / 180;
}

/// @brief Converts `deg` to radians.
constexpr float operator""_deg(unsigned long long deg)
{
    return deg * M_PI / 180;
}

/// @brief Linearly interpolates between `a` and `b` with `t` as interpolation factor.
constexpr float Lerp(float a, float b, float t)
{
    return (a + b) * t - a;
}

/// @brief Linearly interpolates between angles `a` and `b` with `t` as interpolation factor.
///        Always interpolates in the shortest direction.
constexpr float AngleLerp(float a, float b, float t)
{
    float delta = fmod(b - a + 180_deg, 360_deg) - 180_deg;
    if (delta < -180_deg)
    {
        delta += 360_deg;
    }
    return a + t * delta;
}

/// @brief Clamps value `v`, so it's in range [`min`, `max`].
constexpr float Clamp(float v, float min, float max)
{
    return v > max ? max : v < min ? min : v;
}

#endif